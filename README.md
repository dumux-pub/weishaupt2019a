This contains all required files to reproduce the results of [__An efficient coupling of free flow and porous media flow using the pore-network modeling approach__](https://doi.org/10.1016/j.jcpx.2019.100011)

```
@article{weishaupt2019a,
title = "An efficient coupling of free flow and porous media flow using the pore-network modeling approach",
journal = "Journal of Computational Physics: X",
volume = "1",
pages = "100011",
year = "2019",
issn = "2590-0552",
doi = "https://doi.org/10.1016/j.jcpx.2019.100011",
url = "http://www.sciencedirect.com/science/article/pii/S2590055219300277",
author = "Kilian Weishaupt and Vahid Joekar-Niasar and Rainer Helmig"
}
```


Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder && cd New_folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2019a.git
```

After that, execute the file [installWeishaupt2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2019a/raw/master/installWeishaupt2019a.sh)

```
chmod u+x weishaupt2019a/installWeishaupt2019a.sh
./weishaupt2019a/installWeishaupt2019a.sh
```


This should automatically download all necessary modules and check out the correct versions. Furthermore, a patch to `dune-istl` is applied which
lifts `UMFPack`'s memory limitation of 2GB.

Finally, run

```
./dune-common/bin/dunecontrol --opts=dumux/optim.opts all
```

Details
=======



dune-common               master          1e7fbb595155445d4b8cae9a2e1dd0b8e887757c  
dune-geometry             master          a0d66736f9e0e860a44b99357cc8b6958586399d  
dune-grid                 master          8bb63866aa5c0a72f165077038294fa40a5f0816  
dune-localfunctions       master          6e57616d38482ae2923a4bcc36f8d2bc5e0a9b76  
dune-istl                 master          d74bd9c2e40afc96492d8c354620d6c56e8b3df4  
dune-foamgrid             master          0bc28af89974e326005b190d5df4ac8ebb30ebf8  
dune-subgrid              master          e0a3ab0cacaf555dddcb773da031066d46c6b3a4  
dumux                     master          a192b99533ca679701d23be71ae7b5eeb1e04413  
