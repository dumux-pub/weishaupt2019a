// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters for pore network models.
 */
#ifndef DUMUX_PNM_SPATIAL_PARAMS_BASE_HH
#define DUMUX_PNM_SPATIAL_PARAMS_BASE_HH

#include <dumux/porenetworkflow/common/geometry.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dumux/material/spatialparams/fv1p.hh>

#include <dumux/io/grid/griddata.hh>

namespace Dumux
{

/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class FVGridGeometry, class Scalar, class Labels, Shape poreGeometry, Shape throatGeometry, class Implementation>
class PNMBaseSpatialParams
{
    using Grid = typename FVGridGeometry::Grid;
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using CoordScalar = typename GridView::ctype;

    static const int dim = GridView::dimension;
    static const int dimWorld = GridView::dimensionworld;
    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;
    using GlobalPosition = Dune::FieldVector<CoordScalar,dimWorld>;

    using GridData = PoreNetworkGridData<Grid>;

public:
    using PermeabilityType = Scalar;

    PNMBaseSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry, std::shared_ptr<const GridData> gridData)
    : fvGridGeometry_(fvGridGeometry), useBoundaryDataFromSpatialParams_(true), eps_(1e-6), gridData_(gridData)
    {
        boundaryDefinition_ = "Bbox";//getParamFromGroup<std::string>(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Grid.BoundaryDefinition", "Bbox");

        getVertexParamters_();
        getElementParamters_();

        if(boundaryDefinition_ == "IntersectionIterator")
            useBoundaryDataFromSpatialParams_ = false;
    }

    /*!
     * \brief Called by the Problem to initialize the spatial params.
     */
    void init()
    {}

    /*!
     * \brief Harmonic average of a discontinuous scalar field at discontinuity interface
     *        (for compatibility reasons with the function below)
     * \return the averaged scalar
     * \param T1 first scalar parameter
     * \param T2 second scalar parameter
     * \param normal The unit normal vector of the interface
     */
    Scalar harmonicMean(const Scalar T1,
                        const Scalar T2,
                        const GlobalPosition& normal) const
    { return Dumux::harmonicMean(T1, T2); }

    /*************************** Pore Label *************************************************/
    /*!
     * \brief Returns the boundary pore index
     *
     * \param scv The sub control volume
     */
    int poreLabel(const SubControlVolume &scv) const
    {
        return poreLabel_[scv.dofIndex()];
    }

    /*!
     * \brief Returns the boundary pore index.
     *
     * \param dofIdxGlobal The global dof index of the pore
     */
    int poreLabel(const int dofIdxGlobal) const
    {
        return poreLabel_[dofIdxGlobal];
    }
    /****************************************************************************************/

    /*************************** Pore Radius ************************************************/
    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param vertex The vertex
     */
    template<class VolumeVariables>
    Scalar poreRadius(const SubControlVolume &scv, const VolumeVariables& volVars) const
    {
        return poreRadius_[scv.dofIndex()];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param vertex The vertex
     */
    Scalar poreRadius(const SubControlVolume &scv) const
    {
        return poreRadius_[scv.dofIndex()];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param dofIdxGlobal The global dof index of the pore
     * \note Overload this method to make it solution dependend
     */
    template<class VolumeVariables>
    Scalar poreRadius(const int dofIdxGlobal, const VolumeVariables& volVars) const
    {
        return poreRadius_[dofIdxGlobal];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param dofIdxGlobal The global dof index of the pore
     * \note Overload this method to make it solution dependend
     */
    Scalar poreRadius(const int dofIdxGlobal) const
    {
        return poreRadius_[dofIdxGlobal];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param vertex The vertex
     */
    Scalar initialPoreRadius(const SubControlVolume &scv) const
    {
        return poreRadius_[scv.dofIndex()];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param dofIdxGlobal The global dof index of the pore
     * \note Overload this method to make it solution dependend
     */
    Scalar initialPoreRadius(const int dofIdxGlobal) const
    {
        return poreRadius_[dofIdxGlobal];
    }

    /****************************************************************************************/

    /*************************** Pore Volume ************************************************/
    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    template<class VolumeVariables>
    Scalar poreVolume(const SubControlVolume &scv, const VolumeVariables& volVars) const
    {
        return initialPoreVolume(scv.dofIndex());
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    Scalar poreVolume(const SubControlVolume &scv) const
    {
        return initialPoreVolume(scv.dofIndex());
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    template<class VolumeVariables>
    Scalar poreVolume(const int dofIdxGlobal, const VolumeVariables& volVars) const
    {
        return initialPoreVolume(dofIdxGlobal);
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    Scalar poreVolume(const int dofIdxGlobal) const
    {
        return initialPoreVolume(dofIdxGlobal);
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    Scalar initialPoreVolume(const SubControlVolume &scv) const
    {
        return initialPoreVolume(scv.dofIndex());
    }


    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    Scalar initialPoreVolume(const int dofIdxGlobal) const
    {
        if(poreGeometry == Shape::SquarePrism)
            return cubicPoreVolume(poreRadius(dofIdxGlobal));
        else if(poreGeometry == Shape::Sphere)
            return sphericPoreVolume(poreRadius(dofIdxGlobal));
        else if(poreGeometry == Shape::TwoPlates)
            return poreRadius(dofIdxGlobal)*poreRadius(dofIdxGlobal);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid pore geometry");
    }

    /****************************************************************************************/

    /*************************** Throat Label ***********************************************/
    /*!
     * \brief Returns an index indicating if a throat is touching the domain boundary.
     *
     * \param element The throat
     */
    int throatLabel(const Element &element) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatLabel_[eIdx];
    }

    /*!
     * \brief Returns an index indicating if a throat is touching the domain boundary.
     *
     * \param eIdx The throat index
     */
    int throatLabel(const int eIdx) const
    {
        return throatLabel_[eIdx];
    }
    /****************************************************************************************/

    /*************************** Throat Radius **********************************************/
    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    template<class ElementVolumeVariables>
    Scalar throatRadius(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar throatRadius(const Element& element) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar throatRadius(const int eIdx) const
    {
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    template<class ElementVolumeVariables>
    Scalar throatRadius(const int eIdx, const ElementVolumeVariables& elemVolVars) const
    {
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar initialThroatRadius(const Element& element) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar initialThroatRadius(const int eIdx) const
    {
        return throatRadius_[eIdx];
    }


    /****************************************************************************************/

    /*************************** Throat Length **********************************************/
    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    template<class ElementVolumeVariables>
    Scalar throatLength(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    Scalar throatLength(const Element& element) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the throat lenght
     *
     * \param element The element index
     */
    Scalar throatLength(const int eIdx) const
    {
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    template<class ElementVolumeVariables>
    Scalar initialThroatLength(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    Scalar initialThroatLength(const Element& element) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the throat lenght
     *
     * \param element The element index
     */
    Scalar initialThroatLength(const int eIdx) const
    {
        return throatLength_[eIdx];
    }
    /****************************************************************************************/

    /*************************** Throat Cross Section ***************************************/
    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    template<class ElementVolumeVariables>
    Scalar throatCrossSection(const Element& element, const ElementVolumeVariables& elemVolVars, const int phaseIdx = 0) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar throatCrossSection(const Element& element, const int phaseIdx = 0) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param eIdx The element index.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar throatCrossSection(const int eIdx) const
    {
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    template<class ElementVolumeVariables>
    Scalar initialThroatCrossSection(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar initialThroatCrossSection(const Element& element) const
    {
        int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param eIdx The element index.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar initialThroatCrossSection(const int eIdx) const
    {
        const Scalar r = throatRadius_[eIdx];
        if(throatGeometry == Shape::SquarePrism)
            return 4*r*r;
        else if(throatGeometry == Shape::CirclePrism)
            return M_PI*r*r;
        else if(throatGeometry == Shape::TwoPlates)
            return 2*r;
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid throat geometry");
    }

    /****************************************************************************************/

    /*!
     * \brief Returns whether boundary information from the spatialParams should be used for the model's
     *        onBoundary() function
     */
    bool useBoundaryDataFromSpatialParams() const
    { return useBoundaryDataFromSpatialParams_; }

    /*!
     * \brief Return the number of adjacent pore throat for each pore body
     *
     * \param vIdx The vertex index
     */
    int numAdjacentThroats(const int dofIdxGlobal) const
    {
        return numAdjacentThroats_[dofIdxGlobal];
    }

    /*!
     * \brief Returns whether a pore serves as inlet
     *
     * \param vertex The vertex
     */
    bool isInletPore(const SubControlVolume &scv) const
    {
        const int vIdx = scv.dofIndex();
        return isInletPore(vIdx);
    }

    /*!
     * \brief Returns whether a pore serves as inlet
     *
     * \param element The element
     */
    bool isInletPore(const int dofIdxGlobal) const
    {
        return poreLabel_[dofIdxGlobal] == Labels::inlet;
    }

    /*!
     * \brief Returns whether a pore serves as outlet
     *
     * \param vertex The vertex
     */
    bool isOutletPore(const SubControlVolume &scv) const
    {
        const int vIdx = scv.dofIndex();
        return isOutletPore(vIdx);
    }

    /*!
     * \brief Returns whether a pore serves as outlet
     *
     * \param element The element
     */
    bool isOutletPore(const int dofIdxGlobal) const
    {
        return poreLabel_[dofIdxGlobal] == Labels::outlet;
    }

    /*!
     * \brief Returns whether a throat serves as inlet
     *
     * \param element The element
     */
    bool isInletThroat(const Element &element) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatLabel_[eIdx] == Labels::inlet;
    }

    /*!
     * \brief Returns whether a throat serves as inlet
     *
     * \param eIdx The element index
     */
    bool isInletThroat(const int eIdx) const
    {
        return throatLabel_[eIdx] == Labels::inlet;
    }

    /*!
     * \brief Returns whether a throat serves as outlet
     *
     * \param element The element
     */
    bool isOutletThroat(const Element &element) const
    {
        const int eIdx = fvGridGeometry().gridView().indexSet().index(element);
        return throatLabel_[eIdx] == Labels::outlet;
    }

    /*!
     * \brief Returns whether a throat serves as outlet
     *
     * \param eIdx The element index
     */
    bool isOutletThroat(const int eIdx) const
    {
        return throatLabel_[eIdx] == Labels::outlet;
    }

    /*!
     * \brief Returns a reference to the gridview
     *
     */
    const GridView &gridView() const
    {
        return fvGridGeometry().gridView();
    }


     /*!
     * \brief Returns the minumum pore radius
     *
     */
    const Scalar minPoreRadius() const
    {
        return *std::min_element(poreRadius_.begin(), poreRadius_.end());
    }

     /*!
     * \brief Returns the maximum pore radius
     *
     */
    const Scalar maxPoreRadius() const
    {
        return *std::max_element(poreRadius_.begin(), poreRadius_.end());
    }


     /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolutionVector>
    Scalar permeability(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolutionVector& elemSol) const
    { return 1.0; }


        /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The position of the center of the element
     *
     *TODO this is a themporary fix because the implicit VolumeVariables need that.
     * We have a different non-isothermal localresidual so that this is never used because
     * we do not have heat storage in the matrix
     */
    template<class ElementSolutionVector>
    Scalar solidHeatCapacity(const Element &element,
                             const SubControlVolume& scv,
                             const ElementSolutionVector &elemSol) const
    {
        return 0;
    }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The position of the center of the element
     *TODO this is a themporary fix because the implicit VolumeVariables need that.
     * We have a different non-isothermal localresidual so that this is never used because
     * we do not have heat storage in the matrix
     */
    template<class ElementSolutionVector>
    Scalar solidDensity(const Element &element,
                        const SubControlVolume& scv,
                        const ElementSolutionVector &elemSol) const
    {
        return 0;
    }

     /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     *
     * \param globalPos The position of the center of the element
     *TODO this is a themporary fix because the implicit VolumeVariables need that.
     * We have a different non-isothermal localresidual so that this is never used because
     * we do not have heat storage in the matrix
     */
    template<class ElementSolutionVector>
    Scalar solidThermalConductivity(const Element &element,
                                    const SubControlVolume& scv,
                                    const ElementSolutionVector &elemSol) const
    {
        return 0;
    }

    //! The finite volume grid geometry
    const FVGridGeometry& fvGridGeometry() const
    {
        return *fvGridGeometry_;
    }

    /*!
     * \brief Function for defining the porosity.
     *        That is possibly solution dependent.
     * \note this can only be used for solids with one inert component
     *       (see inertVolumeFraction for the more general interface)
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return the porosity
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        static_assert(decltype(isValid(Detail::hasPorosityAtPos<GlobalPosition>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         Scalar porosityAtPos(const GlobalPosition& globalPos) const\n\n"
        "   or overload this function\n\n"
        "         template<class ElementSolution>\n"
        "         Scalar porosity(const Element& element,\n"
        "                         const SubControlVolume& scv,\n"
        "                         const ElementSolution& elemSol) const\n\n");

        return asImp_().porosityAtPos(scv.center());
    }

    /*!
     * \brief Function for defining the solid volume fraction.
     *        That is possibly solution dependent.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \param compIdx The solid component index
     * \return the volume fraction of the inert solid component with index compIdx
     *
     * \note this overload is enable if there is only one inert solid component and the
     *       user didn't choose to implement a inertVolumeFractionAtPos overload.
     *       It then forwards to the simpler porosity interface.
     *       With more than one solid components or active solid components (i.e. dissolution)
     *       please overload the more general inertVolumeFraction/inertVolumeFractionAtPos interface.
     */
    template<class SolidSystem, class ElementSolution,
             typename std::enable_if_t<SolidSystem::isInert()
                                       && SolidSystem::numInertComponents == 1
                                       && !decltype(isValid(Detail::hasInertVolumeFractionAtPos<GlobalPosition, SolidSystem>())(std::declval<Implementation>()))::value,
                                       int> = 0>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        return 1.0 - asImp_().porosity(element, scv, elemSol);
    }

    // specialization if there are no inert components at all
    template<class SolidSystem, class ElementSolution,
             typename std::enable_if_t<SolidSystem::numInertComponents == 0, int> = 0>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        return 0.0;
    }

    // the more general interface forwarding to inertVolumeFractionAtPos
    template<class SolidSystem, class ElementSolution,
             typename std::enable_if_t<(SolidSystem::numInertComponents > 1) ||
                                       (
                                            (SolidSystem::numInertComponents > 0) &&
                                            (
                                                !SolidSystem::isInert()
                                                || decltype(isValid(Detail::hasInertVolumeFractionAtPos<GlobalPosition, SolidSystem>())
                                                        (std::declval<Implementation>()))::value
                                            )
                                        ),
                                        int> = 0>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        static_assert(decltype(isValid(Detail::hasInertVolumeFractionAtPos<GlobalPosition, SolidSystem>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         template<class SolidSystem>\n"
        "         Scalar inertVolumeFractionAtPos(const GlobalPosition& globalPos, int compIdx) const\n\n"
        "   or overload this function\n\n"
        "         template<class SolidSystem, class ElementSolution>\n"
        "         Scalar inertVolumeFraction(const Element& element,\n"
        "                                    const SubControlVolume& scv,\n"
        "                                    const ElementSolution& elemSol,\n"
        "                                    int compIdx) const\n\n");

        return asImp_().template inertVolumeFractionAtPos<SolidSystem>(scv.center(), compIdx);
    }

protected:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

private:

     /*!
     * \brief Retrieves the vertex-related pore paramaters from the GridCreator. Can be overloaded for using special parameters
     *
     */
    void getVertexParamters_()
    {
        const auto numPores = gridView().size(dim);
        poreRadius_.resize(numPores);
        poreLabel_.resize(numPores);
        numAdjacentThroats_.resize(numPores);

        const auto poreRadiusIdx = gridData_->index(GridData::VertexParameter::radius);
        const auto poreLabelIdx = gridData_->index(GridData::VertexParameter::label);

        numAdjacentThroats_ = gridData_->getCoordinationNumbers();

        for(const auto& vertex : vertices(gridView()))
        {
            const int vIdx = gridView().indexSet().index(vertex);
            const auto& params = gridData_->parameters(vertex);
            poreRadius_[vIdx] = params[poreRadiusIdx];
            assert(poreRadius_[vIdx] > 0.0);
            poreLabel_[vIdx] = params[poreLabelIdx];
        }

        // free some memory
        // if(!isDgf)
            // GridCreator::deleteVertexParamterContainers();
    }

     /*!
     * \brief Retrieves the element-related throat paramaters from the GridCreator. Can be overloaded for using special parameters
     *
     */
    void getElementParamters_()
    {
        const auto numThroats = gridView().size(0);
        throatRadius_.resize(numThroats);
        throatLength_.resize(numThroats);
        throatLabel_.resize(numThroats);

        const auto throatRadiusIdx = gridData_->index(GridData::ElementParameter::radius);
        const auto throatLengthIdx = gridData_->index(GridData::ElementParameter::length);

        const auto throatLabel = [&] (const auto& element)
        {
            if(gridData_->parameters(element).size() < 3) // DGF file does not specify throatLabel
                return gridData_->throatLabel(element);
            else
                return static_cast<int>(gridData_->parameters(element)[gridData_->index(GridData::ElementParameter::label)]);
        };

        for(const auto& element : elements(gridView()))
        {
            const int eIdx = gridView().indexSet().index(element);
            const auto params = gridData_->parameters(element);

            throatRadius_[eIdx] = params[throatRadiusIdx];
            throatLength_[eIdx] = params[throatLengthIdx];
            assert(throatRadius_[eIdx] > 0.0);
            assert(throatLength_[eIdx] > 0.0);
            throatLabel_[eIdx] = throatLabel(element);
        }

        // free some memory
        // if(!isDgf)
            // GridCreator::deleteElementParamterContainers();
    }

    std::shared_ptr<const FVGridGeometry> fvGridGeometry_;
    bool useBoundaryDataFromSpatialParams_;

    const Scalar eps_;
    // TODO: use shared pointers!
    std::vector<Scalar> poreRadius_;
    std::vector<int> poreLabel_; // 0:no, 1:general, 2:coupling1, 3:coupling2, 4:inlet, 5:outlet
    std::vector<int> throatLabel_; // 0:no, 1:general, 2:coupling1, 3:coupling2, 4:inlet, 5:outlet
    std::vector<Scalar> throatRadius_;
    std::vector<Scalar> throatLength_;
    std::vector<unsigned int> numAdjacentThroats_;
    std::string boundaryDefinition_;

    std::shared_ptr<const GridData> gridData_;
};

} // namespace Dumux

#endif
