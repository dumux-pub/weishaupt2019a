// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        the fluxes of the pore network model over a face of a finite volume.
 *
 * This means pressure gradients, phase densities at the integration point, etc.
 */
#ifndef DUMUX_PNM_ADVECTIVE_FLUX_HH
#define DUMUX_PNM_ADVECTIVE_FLUX_HH

#include <cmath>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <type_traits>
#include <dumux/discretization/methods.hh>

namespace Dumux
{

// forward declaration
template <class Scalar, class TransmissibilityType, DiscretizationMethod Method>
class WashburnAdvection
{};


template<class Scalar, class TransmissibilityType>
class WashburnAdvection<Scalar, TransmissibilityType, DiscretizationMethod::box>
{

public:
    template<class Problem, class Element, class FVElementGeometry,
             class ElementVolumeVariables, class SubControlVolumeFace, class ElemFluxVarsCache>
    static Scalar flux(const Problem& problem,
                       const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolumeFace& scvf,
                       const int phaseIdx,
                       const ElemFluxVarsCache& elemFluxVarsCache)
    {
        const auto& fluxVarsCache = elemFluxVarsCache[scvf];

        // Get the inside and outside volume variables
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
        const auto& insideVolVars = elemVolVars[insideScv];
        const auto& outsideVolVars = elemVolVars[outsideScv];

        // calculate the pressure difference
        const Scalar deltaP = insideVolVars.pressure(phaseIdx) - outsideVolVars.pressure(phaseIdx);
        const Scalar transmissibility = fluxVarsCache.transmissibility(phaseIdx);
        return transmissibility*deltaP;
    }

    /*!
     * \brief Returns the throat conductivity
     *
     * \param problem The problem
     * \param element The element
     * \param ElementVolumeVariables The element volume variables
     */
    template<class Problem, class Element, class ElementVolumeVariables>
    static Scalar calculateTransmissibility(const Problem &problem,
                                            const Element &element,
                                            const ElementVolumeVariables &elemVolVars,
                                            const int phaseIdx)
    {
        return TransmissibilityType::transmissibility(problem, element, elemVolVars, phaseIdx);
    }
};


} // end namespace

#endif
