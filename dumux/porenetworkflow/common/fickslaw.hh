// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        diffusive mass fluxes due to molecular diffusion with Fick's law.
 */
#ifndef DUMUX_PNM_FICKS_LAW_HH
#define DUMUX_PNM_FICKS_LAW_HH

#include <dune/common/float_cmp.hh>

#include <dumux/common/math.hh>
#include <dumux/common/parameters.hh>

#include <dumux/common/properties.hh>
#include <dumux/discretization/fluxvariablescaching.hh>

namespace Dumux
{

/*!
 * \ingroup PNMFicksLaw
 * \brief Specialization of Fick's Law for the pore-network model.
 */
template <class TypeTag>
class PNMFicksLaw
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using EffDiffModel = typename GET_PROP_TYPE(TypeTag, EffectiveDiffusivityModel);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using ElementFluxVariablesCache = typename GET_PROP_TYPE(TypeTag, GridFluxVariablesCache)::LocalView;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using IndexType = typename GridView::IndexSet::IndexType;

    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int numComponents = GET_PROP_TYPE(TypeTag, ModelTraits)::numComponents();

    using GlobalPosition = Dune::FieldVector<Scalar, GridView::dimensionworld>;
    using ComponentFluxVector = Dune::FieldVector<Scalar, numComponents>;

public:
    static ComponentFluxVector flux(const Problem& problem,
                                    const Element& element,
                                    const FVElementGeometry& fvGeometry,
                                    const ElementVolumeVariables& elemVolVars,
                                    const SubControlVolumeFace& scvf,
                                    const int phaseIdx,
                                    const ElementFluxVariablesCache& elemFluxVarsCache)
    {
        ComponentFluxVector componentFlux(0.0);

        // get inside and outside diffusion tensors and calculate the harmonic mean
        const auto& insideVolVars = elemVolVars[scvf.insideScvIdx()];
        const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];

        auto& spatialParams = problem.spatialParams();
        const Scalar molarDensity = 0.5 * (insideVolVars.molarDensity(phaseIdx) + outsideVolVars.molarDensity(phaseIdx));
        const Scalar throatLength = spatialParams.throatLength(element, elemVolVars);
        const Scalar throatCrossSection = spatialParams.throatCrossSection(element, elemVolVars, phaseIdx);

        for (int compIdx = 0; compIdx < numComponents; compIdx++)
        {
            if(compIdx == phaseIdx)
                continue;

            // effective diffusion tensors
            auto insideD = EffDiffModel::effectiveDiffusivity(insideVolVars.porosity(),
                                                              insideVolVars.saturation(phaseIdx),
                                                              insideVolVars.diffusionCoefficient(phaseIdx, compIdx));

            auto outsideD = EffDiffModel::effectiveDiffusivity(outsideVolVars.porosity(),
                                                               outsideVolVars.saturation(phaseIdx),
                                                               outsideVolVars.diffusionCoefficient(phaseIdx, compIdx));

            // scale by extrusion factor
            insideD *= insideVolVars.extrusionFactor();
            outsideD *= outsideVolVars.extrusionFactor();


            // the resulting averaged diffusion tensor
            const auto D = spatialParams.harmonicMean(insideD, outsideD, scvf.unitOuterNormal());

            const Scalar insideMoleFraction = insideVolVars.moleFraction(phaseIdx, compIdx);
            const Scalar outsideMoleFraction = outsideVolVars.moleFraction(phaseIdx, compIdx);

            componentFlux[compIdx] = molarDensity * (insideMoleFraction - outsideMoleFraction) / throatLength * D * throatCrossSection;
            componentFlux[phaseIdx] -= componentFlux[compIdx];
        }
        return componentFlux;
    }

};
} // end namespace

#endif
