// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains functions useful for all types of pore-network models,
 *        e.g. for the calculation of fluxes at the boundary.
 *
 */
#ifndef DUMUX_PNM_GEOMETRY_HH
#define DUMUX_PNM_GEOMETRY_HH

#include <cmath>
#include <numeric>
#include "labels.hh"

namespace Dumux
{

enum class Shape : unsigned int
{
    SquarePrism, CirclePrism, TrianglePrism, Sphere, EllipsePrism, RectanglePrism, TwoPlates
};

// free functions for different geometric relations

 /*!
 * \brief Returns the volume of a cubic pore
 *
 * \param radius The radius of a inscribed sphere
 */
template<class Scalar>
inline Scalar cubicPoreVolume(const Scalar radius)
{
    return 8*radius*radius*radius;
}

 /*!
 * \brief Returns the volume of a spheric pore
 *
 * \param radius The radius of the pore
 */
template<class Scalar>
inline Scalar sphericPoreVolume(const Scalar radius)
{
    return 4.0/3.0*M_PI*radius*radius*radius;
}

 /*!
 * \brief Returns the volume of a cylindric throat
 *
 * \param length The throat length
 * \param radius The throat radius
 */
template<class Scalar>
inline Scalar cylindricThroatVolume(const Scalar length, const Scalar radius)
{
    return M_PI*radius*radius*length;
}

 /*!
 * \brief Returns the total cumulative pore volume
 *
 * \param poreVolume Vector of all pore volumes
 * \param poreLabels Vector of all pore labels (optional. If not given, all pores (including those
 *                   on the domain boundary) will be considered for the calculation of the total pore volume.
 */
template<class Scalar>
inline Scalar totalPoreBodyVolume(const std::vector<Scalar> &poreVolume,
                                  const std::vector<int> &poreLabels = std::vector<int>())
{
    Scalar poreBodyVolume(0);
    const bool omitPoresOnBoundary = poreVolume.size() == poreLabels.size() ? true : false;
    for(int vIdx = 0; vIdx < poreVolume.size(); ++vIdx)
    {
        if(omitPoresOnBoundary && poreLabels[vIdx] != Dumux::Labels::interior)
            continue;
        else
            poreBodyVolume += poreVolume[vIdx];
    }
    return poreBodyVolume;
}

 /*!
 * \brief Returns the total cumulative throat volume
 *
 * \param throatVolume Vector of all throat volumes
 */
template<class Scalar>
inline Scalar totalThroatVolume(const std::vector<Scalar> &throatVolume)
{
    return std::accumulate(throatVolume.begin(), throatVolume.end(), 0.0);
}

 /*!
 * \brief Calculates the bulk porosity
 *
 * \param totalBulkVolume The total bulk volume of the domain
 * \param poreVolume Vector of all pore volumes
 * \param poreLabels Vector of all pore labels (optional. If not given, all pores (including those
 *                   on the domain boundary) will be considered for the calculation of the total pore volume.
 * \param throatVolume Vector of all throat volumes (optional). If not given, the throat's volumes are not included.
 */
template<class Scalar>
inline Scalar calculateBulkPorosity(const Scalar totalBulkVolume,
                                const std::vector<Scalar> &poreVolume,
                                const std::vector<int> &poreLabels = std::vector<int>(),
                                const std::vector<Scalar> &throatVolume = std::vector<Scalar>())
{
    Scalar porosity = totalPoreBodyVolume(poreVolume, poreLabels);
    if(!throatVolume.empty())
        porosity += totalThroatVolume(throatVolume);
    return porosity /= totalBulkVolume;
}

 /*!
 * \brief Returns the radius of a pore throat
 *
 * \param poreRadiusOne The radius of the first pore
 * \param poreRadiusTwo The radius of the second pore
 * \param centerTocenterDist The center-to-center distance between the pores
 * \param n Fitting parameter
 *
 * Joekar-Niasar et al. (2008) \cite joekar-niasar2008
 */
template<class Scalar>
inline Scalar averagedThroatRadius(const Scalar poreRadiusOne, const Scalar poreRadiusTwo, const Scalar centerTocenterDist, const Scalar n = 0.1)
{
    assert(n > 0.0);
    const Scalar rOneTilde = poreRadiusOne/centerTocenterDist;
    const Scalar rTwoTilde = poreRadiusTwo/centerTocenterDist;
    const Scalar a = std::sin(M_PI/4.0);
    const Scalar b = std::cos(M_PI/4.0);
    const Scalar rhoOne = rOneTilde*a / std::pow((1.0 - rOneTilde*b), n);
    const Scalar rhoTwo = rTwoTilde*a / std::pow((1.0 - rTwoTilde*b), n);
    const Scalar rTilde = rhoOne*rhoTwo * std::pow((std::pow(rhoOne, 1.0/n) + std::pow(rhoTwo, 1.0/n)), -n);
    return rTilde * centerTocenterDist;
}

 /*!
 * \brief Returns the crevice resistance factor used for calculating the w-phase conductance in an invaded pore throat
 *
 * \param alpha The corner half angle
 * \param theta The contact angle
 * \param f The boundary condition factor for the fluid interface (0 = free boundary)
 *
 * Zhou et al. (1997) \cite zhou_et_al1997
 */
template<class Scalar>
inline Scalar beta(const Scalar alpha, const Scalar theta, const Scalar f = 0)
{
    const Scalar sinAlpha = std::sin(alpha);
    const Scalar sinSum = std::sin(alpha + theta);
    const Scalar cosSum = std::cos(alpha + theta);
    const Scalar phi1 = cosSum*cosSum + cosSum*sinSum*std::tan(alpha);
    const Scalar phi2 = 1 - theta/(M_PI/2 - alpha);
    const Scalar phi3 = cosSum / std::cos(alpha);
    const Scalar B = (M_PI/2 - alpha)*std::tan(alpha);

    Scalar result = 12*sinAlpha*sinAlpha*(1-B)*(1-B)*(phi1 - B*phi2)*(phi3 + f*B*phi2)*(phi3 + f*B*phi2);
    result /= (1-sinAlpha)*(1-sinAlpha)*B*B*(phi1 - B*phi2)*(phi1 - B*phi2)*(phi1 - B*phi2);
    return result;
}

/*!
* \brief Returns the projected radius of a throat cutting a plane
*
* \param throatRadius The throat's actual radius
* \param element The element
* \param cutPlaneNormal The plane's normal vector
*
*/
template<class Scalar, class Element>
inline auto projectedThroatRadius(const Scalar& throatRadius,
                                  const Element& element,
                                  const typename Element::Geometry::GlobalCoordinate& cutPlaneNormal)
{
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static_assert(GlobalPosition::dimension == 2, "Projection of throat radius only works in 2D");

    // determine the throat radius, which might depend on the throat's angle of orientation
    const GlobalPosition throatOrientationVector = element.geometry().corner(1) - element.geometry().corner(0);

    // get the angle between the throat and the plane it cuts
    const Scalar cosPhi = cutPlaneNormal * throatOrientationVector / (cutPlaneNormal.two_norm() * throatOrientationVector.two_norm());
    const Scalar phi = std::acos(cosPhi); // in radians
    const Scalar alpha = phi > 0.5*M_PI ? phi - 0.5*M_PI : 0.5*M_PI - phi; // 0.5*pi rad == 90 deg; we define alpha always smaller than 90 deg

    // get the radius projected onto the plane the throat cuts
    return throatRadius / std::sin(alpha);
}


} // end namespace

#endif
