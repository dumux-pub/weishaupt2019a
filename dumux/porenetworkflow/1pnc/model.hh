// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief  Adaption of the fully implicit scheme to the one-phase n-component pore network model.
 */

#ifndef DUMUX_PNM1PNC_MODEL_HH
#define DUMUX_PNM1PNC_MODEL_HH

#include <dumux/common/properties.hh>
#include <dumux/porenetworkflow/properties.hh>

#include <dumux/porenetworkflow/1p/model.hh>
#include <dumux/material/spatialparams/porenetwork/porenetwork1p.hh>

#include <dumux/porousmediumflow/compositional/localresidual.hh>
#include <dumux/porousmediumflow/nonisothermal/model.hh>
#include <dumux/porousmediumflow/nonisothermal/indices.hh>
#include <dumux/porousmediumflow/nonisothermal/vtkoutputfields.hh>

#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/porousmediumflow/1pnc/vtkoutputfields.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivitymillingtonquirk.hh>

#include <dumux/material/spatialparams/porenetwork/porenetwork1p.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility.hh>

#include <dumux/material/fluidstates/immiscible.hh>

#include "vtkoutputfields.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tags for the implicit single-phase mulit-component problems
NEW_TYPE_TAG(PNMOnePNC, INHERITS_FROM(OnePNC, PoreNetworkModel));

//! The type tags for the corresponding non-isothermal problems
NEW_TYPE_TAG(PNMOnePNCNI, INHERITS_FROM(PNMOnePNC));

///////////////////////////////////////////////////////////////////////////
// properties for the isothermal single phase model
///////////////////////////////////////////////////////////////////////////

//! The spatial parameters to be employed.
//! Use PNMOnePSpatialParams by default.
SET_PROP(PNMOnePNC, SpatialParams)
{
private:
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
public:
    using type = PNMOnePSpatialParams<FVGridGeometry, Scalar, Labels, GET_PROP_VALUE(TypeTag, PoreGeometry), GET_PROP_VALUE(TypeTag, ThroatGeometry)>;
};

//! use a square-shaped pore cross-section as default
SET_PROP(PNMOnePNC, PoreGeometry)
{
     static const Shape value = Shape::SquarePrism;
//     static const Shape value = Shape::Sphere;
};

//! use a square-shaped throat cross-section as default
SET_PROP(PNMOnePNC, ThroatGeometry)
{
     static const Shape value = Shape::SquarePrism;
//     static const ThroatGeometry value = ThroatGeometry::CirclePrism;
};

//! the default transmissibility law
// SET_TYPE_PROP(PNMOneP, SinglePhaseTransmissibility, TransmissibilityAzzamDullien<TypeTag>);
SET_TYPE_PROP(PNMOnePNC, SinglePhaseTransmissibility, TransmissibilityBruus<TypeTag, false>);

//! Set as default that no component mass balance is replaced by the total mass balance
SET_PROP(PNMOnePNC, ReplaceCompEqIdx)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem));
public:
    static constexpr auto value = FluidSystem::numComponents;
};

//! The local residual function
SET_TYPE_PROP(PNMOnePNC, LocalResidual, PNMLocalResidual<TypeTag, CompositionalLocalResidual<TypeTag> >);

//!< Set the vtk output fields specific to this model
SET_PROP(PNMOnePNC, VtkOutputFields)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
public:
    using type = PNMOnePNCVtkOutputFields<FluidSystem>;
};

SET_BOOL_PROP(PNMOnePNC, UseMoles, true);

//////////////////////////////////////////////////////////////////
// Property values for isothermal model required for the general non-isothermal model
//////////////////////////////////////////////////////////////////

//! model traits of the non-isothermal model.
SET_PROP(PNMOnePNCNI, ModelTraits)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem));
    using IsothermalTraits = OnePNCModelTraits<FluidSystem::numComponents, GET_PROP_VALUE(TypeTag, UseMoles), GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx)>;
public:
    using type = PorousMediumFlowNIModelTraits<IsothermalTraits>;
};

SET_PROP(PNMOnePNCNI, VtkOutputFields)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using IsothermalFields = PNMOnePNCVtkOutputFields<FluidSystem>;
public:
    using type = EnergyVtkOutputFields<IsothermalFields>;
};

SET_TYPE_PROP(PNMOnePNCNI,
              ThermalConductivityModel,
              ThermalConductivityAverage<typename GET_PROP_TYPE(TypeTag, Scalar)>); //!< Use the average for effective conductivities
}

}

#endif
