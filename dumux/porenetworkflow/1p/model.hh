// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Base class for all models which use the one-phase,
 *        fully implicit model.
 *        Adaption of the fully implicit scheme to the one-phase pore network model.
 */

#ifndef DUMUX_PNM1P_MODEL_HH
#define DUMUX_PNM1P_MODEL_HH


#include <dumux/common/properties.hh>
#include <dumux/porenetworkflow/properties.hh>

#include <dumux/porousmediumflow/immiscible/localresidual.hh>
#include <dumux/porousmediumflow/nonisothermal/model.hh>
#include <dumux/porousmediumflow/nonisothermal/vtkoutputfields.hh>

#include <dumux/porousmediumflow/1p/model.hh>

#include <dumux/material/spatialparams/porenetwork/porenetwork1p.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility.hh>

#include "vtkoutputfields.hh"

namespace Dumux
{
/*!
 * \ingroup Pore network model
 * \brief A one-phase, isothermal flow model using the fully implicit scheme.
 *
 * one-phase, isothermal flow model, which uses a standard Darcy approach as the
 * equation for the conservation of momentum:
 * \f[
 v = - \frac{\textbf K}{\mu}
 \left(\textbf{grad}\, p - \varrho {\textbf g} \right)
 * \f]
 *
 * and solves the mass continuity equation:
 * \f[
 \phi \frac{\partial \varrho}{\partial t} + \text{div} \left\lbrace
 - \varrho \frac{\textbf K}{\mu} \left( \textbf{grad}\, p -\varrho {\textbf g} \right) \right\rbrace = q,
 * \f]
 * All equations are discretized using a vertex-centered finite volume (box)
 * or cell-centered finite volume scheme as spatial
 * and the implicit Euler method as time discretization.
 * The model supports compressible as well as incompressible fluids.
 */

 ///////////////////////////////////////////////////////////////////////////
 // properties for the isothermal single phase model
 ///////////////////////////////////////////////////////////////////////////
 namespace Properties {

 //////////////////////////////////////////////////////////////////
 // Type tags
 //////////////////////////////////////////////////////////////////

//! The type tags for the implicit single-phase problems
NEW_TYPE_TAG(PNMOneP, INHERITS_FROM(OneP, PoreNetworkModel));

//! The type tags for the corresponding non-isothermal problems
NEW_TYPE_TAG(PNMOnePNI, INHERITS_FROM(PNMOneP));

//! The local residual function
SET_TYPE_PROP(PNMOneP, LocalResidual, PNMLocalResidual<TypeTag, ImmiscibleLocalResidual<TypeTag> >);

//! The spatial parameters to be employed.
//! Use PNMOnePSpatialParams by default.
SET_PROP(PNMOneP, SpatialParams)
{
private:
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
public:
    using type = PNMOnePSpatialParams<FVGridGeometry, Scalar, Labels, GET_PROP_VALUE(TypeTag, PoreGeometry), GET_PROP_VALUE(TypeTag, ThroatGeometry)>;
};


SET_TYPE_PROP(PNMOneP, VtkOutputFields, PNMOnePVtkOutputFields);

//! use the standard IntersectionIterator to determine boundaries
SET_STRING_PROP(PNMOneP, GridBoundaryDefinition, "Bbox");
// SET_STRING_PROP(PNMOneP, GridBoundaryDefinition, "IntersectionIterator");


//! use a square-shaped pore cross-section as default
SET_PROP(PNMOneP, PoreGeometry)
{
     static const Shape value = Shape::SquarePrism;
//     static const Shape value = Shape::Sphere;
};

//! use a square-shaped throat cross-section as default
SET_PROP(PNMOneP, ThroatGeometry)
{
     static const Shape value = Shape::SquarePrism;
//     static const ThroatGeometry value = ThroatGeometry::CirclePrism;
};

//! the default transmissibility law
// SET_TYPE_PROP(PNMOneP, SinglePhaseTransmissibility, TransmissibilityAzzamDullien<TypeTag>);
SET_TYPE_PROP(PNMOneP, SinglePhaseTransmissibility, TransmissibilityBruus<TypeTag, false>);

//////////////////////////////////////////////////////////////////
// Property values for isothermal model required for the general non-isothermal model
//////////////////////////////////////////////////////////////////

SET_TYPE_PROP(PNMOnePNI, VtkOutputFields, EnergyVtkOutputFields<PNMOnePVtkOutputFields>); //!< Add temperature to the output

SET_TYPE_PROP(PNMOnePNI, ModelTraits, PorousMediumFlowNIModelTraits<OnePModelTraits>); //!< The model traits of the non-isothermal model

SET_TYPE_PROP(PNMOnePNI,
              ThermalConductivityModel,
              ThermalConductivityAverage<typename GET_PROP_TYPE(TypeTag, Scalar)>); //!< Use the average for effective conductivities

} // end namespace Properies

} //namespace Dumux


#endif
