// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoubdaryCoupling
 * \ingroup BoxModel
 * \copydoc Dumux::PNMStokesCouplingManager
 */

#ifndef DUMUX_PNM_STOKES_COUPLINGMAPPER_HH
#define DUMUX_PNM_STOKES_COUPLINGMAPPER_HH

#include <iostream>
#include <memory>
#include <algorithm>
#include <iterator>

#include <dumux/common/properties.hh>
#include <dumux/porenetworkflow/common/geometry.hh>


namespace Dumux {

/*!
 * \ingroup MixedDimension
 * \ingroup MixedDimensionBoundary
 * \brief Coupling manager for low-dimensional domains coupled at the boundary to the bulk
 *        domain.
 */
template<class MDTraits>
class PNMStokesCouplingMapper
{
    using Scalar = typename MDTraits::Scalar;

public:
    static constexpr auto bulkCellCenterIdx = typename MDTraits::template DomainIdx<0>();
    static constexpr auto bulkIdx = bulkCellCenterIdx;
    static constexpr auto lowDimIdx = typename MDTraits::template DomainIdx<2>();

private:

    // obtain the type tags of the sub problems
    using StokesTypeTag = typename MDTraits::template SubDomainTypeTag<0>;
    using LowDimTypeTag = typename MDTraits::template SubDomainTypeTag<2>;

    // the sub domain type tags
    template<std::size_t id>
    using SubDomainTypeTag = typename MDTraits::template SubDomainTypeTag<id>;

    template<std::size_t id> using GridView = typename GET_PROP_TYPE(SubDomainTypeTag<id>, GridView);
    template<std::size_t id> using FVGridGeometry = typename GET_PROP_TYPE(SubDomainTypeTag<id>, FVGridGeometry);
    template<std::size_t id> using FVElementGeometry = typename FVGridGeometry<id>::LocalView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;

    static constexpr auto dimWorld = GridView<bulkCellCenterIdx>::dimensionworld;

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using CouplingManager = typename GET_PROP_TYPE(StokesTypeTag, CouplingManager);

public:

    //! retruns the relevant coupling maps and stencils
    auto computeCouplingMapsAndStencils(const CouplingManager& couplingManager)
    {
        using std::abs;
        struct PNMStokesMappingData
        {
            using Stencils = typename CouplingManager::CouplingStencils;
            Stencils lowDimToBulkCellCenterStencils;
            Stencils lowDimToBulkFaceStencils;
            Stencils bulkCellCenterToLowDimStencils;
            Stencils bulkFaceToLowDimStencils;

            std::vector<bool> isCoupledLowDimDof;
            std::vector<bool> isCoupledBulkFaceDof;
            std::vector<bool> isCoupledBulkFrontalFaceDof;

            std::unordered_map<std::size_t, std::vector<std::size_t>> lowDimElementToBulkElementsMap;
            std::unordered_map<std::size_t, std::size_t> bulkElementToLowDimElementMap;
        } data;

        const auto& bulkFvGridGeometry = couplingManager.problem(bulkIdx).fvGridGeometry();
        const auto& lowDimFvGridGeometry = couplingManager.problem(lowDimIdx).fvGridGeometry();

        data.isCoupledLowDimDof.resize(lowDimFvGridGeometry.numDofs(), false);
        data.isCoupledBulkFaceDof.resize(bulkFvGridGeometry.numFaceDofs(), false);
        data.isCoupledBulkFrontalFaceDof.resize(bulkFvGridGeometry.numFaceDofs(), false);

        auto lowDimFvGeometry = localView(lowDimFvGridGeometry);
        auto bulkFvGeometry = localView(bulkFvGridGeometry);

        const auto& lowDimProblem = couplingManager.problem(lowDimIdx);

        // iterate over the lowDim elements
        for(const auto& lowDimElement : elements(lowDimFvGridGeometry.gridView()))
        {
            // check if the lowDim vertices intersect with the bulk grid
            lowDimFvGeometry.bindElement(lowDimElement);
            for(const auto& lowDimScv : scvs(lowDimFvGeometry))
            {
                // skip the dof if it is not on the boundary
                if(!dofOnBoundary(lowDimProblem, lowDimScv.dofIndex()))
                    continue;

                // get the intersection bulk element
                const auto lowDimPos = lowDimScv.dofPosition();
                const auto lowDimDofIdx = lowDimScv.dofIndex();

                // check for intersections
                const auto directlyCoupledElementIdx = [&]()
                {
                    const auto tmp = intersectingEntities(lowDimPos, bulkFvGridGeometry.boundingBoxTree());

                    // no intersection found
                    if (tmp.empty())
                        return static_cast<std::size_t>(-99);

                    // sanity check
                    if (tmp.size() > 1)
                        DUNE_THROW(Dune::InvalidStateException, "LowDimDof may only intersect with one bulk element");

                    return tmp[0];
                }();

                // skip if no intersection was found
                if (directlyCoupledElementIdx == -99)
                    continue;
                else
                    data.isCoupledLowDimDof[lowDimDofIdx] = true;

                const auto lowDimElementIdx = lowDimFvGridGeometry.elementMapper().index(lowDimElement);
                const auto& otherLowDimScv = lowDimFvGeometry.scv(1 - lowDimScv.indexInElement());
                const auto otherLowDimScvDofIdx = otherLowDimScv.dofIndex();

                const GlobalPosition couplingPlaneNormal = {0.0, 1.0};

                // determine the throat radius, which might depend on the throat's angle of orientation
                const Scalar cutThroatRadius = projectedThroatRadius(lowDimProblem.spatialParams().throatRadius(lowDimElement),
                                                                     lowDimElement, couplingPlaneNormal);
                // std::cout << "at pos " << lowDimPos << " throat radius " << lowDimProblem.spatialParams().throatRadius(lowDimElement) << ", projected radius " << cutThroatRadius << std::endl;

                // treat the directly coupled bulk element
                const auto& directlyCoupledElement = bulkFvGridGeometry.boundingBoxTree().entitySet().entity(directlyCoupledElementIdx);

                const auto couplingPlaneDirectionIdx = [&]()
                {
                    bulkFvGeometry.bindElement(directlyCoupledElement);
                    for (const auto& scvf : scvfs(bulkFvGeometry))
                    {
                        const Scalar eps = scvf.area()*1e-8;
                        if ((scvf.center() - lowDimPos).two_norm() < eps)
                            return scvf.directionIndex();
                    }
                    DUNE_THROW(Dune::InvalidStateException, "No coupled face found. Pore center must coincide with center of scvf");
                }();

                // get all (indirectly coupled bulk elements + directly coupled element)
                const auto allCoupledElementIndices = getAllCoupledElements_(couplingManager, directlyCoupledElement, lowDimPos, cutThroatRadius, couplingPlaneDirectionIdx);

                data.lowDimElementToBulkElementsMap[lowDimElementIdx] = allCoupledElementIndices;

                for (const auto bulkElemIdx : allCoupledElementIndices)
                {
                    if (!data.bulkElementToLowDimElementMap.count(bulkElemIdx))
                        data.bulkElementToLowDimElementMap[bulkElemIdx] = lowDimElementIdx;

                    data.lowDimToBulkCellCenterStencils[lowDimElementIdx].push_back(bulkElemIdx);
                    data.bulkCellCenterToLowDimStencils[bulkElemIdx].push_back(lowDimDofIdx);

                    const auto& bulkElement = bulkFvGridGeometry.boundingBoxTree().entitySet().entity(bulkElemIdx);
                    bulkFvGeometry.bindElement(bulkElement);

                    const auto coupledFaceDofIndices = coupledFaces_(bulkFvGeometry, lowDimPos, cutThroatRadius, couplingPlaneDirectionIdx);

                    data.lowDimToBulkFaceStencils[lowDimElementIdx].push_back(coupledFaceDofIndices.coupledFrontalFace);
                    data.bulkFaceToLowDimStencils[coupledFaceDofIndices.coupledFrontalFace].push_back(lowDimDofIdx);
                    data.bulkFaceToLowDimStencils[coupledFaceDofIndices.coupledFrontalFace].push_back(otherLowDimScvDofIdx);

                    data.isCoupledBulkFaceDof[coupledFaceDofIndices.coupledFrontalFace] = true;
                    data.isCoupledBulkFrontalFaceDof[coupledFaceDofIndices.coupledFrontalFace] = true;

                    // treat the coupled normal faces
                    for (const auto bulkFaceDofIdx : coupledFaceDofIndices.coupledNormalFaces)
                    {
                        data.bulkFaceToLowDimStencils[bulkFaceDofIdx].push_back(lowDimDofIdx);
                        data.bulkFaceToLowDimStencils[bulkFaceDofIdx].push_back(otherLowDimScvDofIdx);
                        data.isCoupledBulkFaceDof[bulkFaceDofIdx] = true;
                    }
                }
            }
        }

        return data;
    }


private:

    //! get the bulk faces that are coupled to the lowDim dofs
    auto coupledFaces_(const FVElementGeometry<bulkIdx>& fvGeometry,
                       const GlobalPosition& lowDimPos,
                       const Scalar cutThroatRadius,
                       const int couplingPlaneDirectionIdx) const
    {
        using std::abs;

        struct Result
        {
            std::vector<std::size_t> coupledNormalFaces;
            std::size_t coupledFrontalFace;
        } result;

        result.coupledNormalFaces.reserve(fvGeometry.numScvf());

        // choose whether to consider the outermost (at the location of the projected throat radius) perpendicular bulk faces
        // or not - the velocity should acutally be zero at this position due to no-flow/no-slip and therefore thoses faces should be neglected
        static const bool considerOuterSides = getParam<bool>("Problem.ConsiderOuterSides", false);

        for (const auto& scvf : scvfs(fvGeometry))
        {
            const Scalar eps = scvf.area()*1e-8;
            assert(eps < cutThroatRadius);

            if (scvf.directionIndex() == couplingPlaneDirectionIdx) // the bulk faces that lie within the coupling interface
            {
                if (scvf.center()[couplingPlaneDirectionIdx] - lowDimPos[couplingPlaneDirectionIdx] < eps)
                    result.coupledFrontalFace = scvf.dofIndex();
            }
            else // the bulk faces perpendicular to the coupling interface
            {
                bool isCoupledFace = true;

                for (int dimIdx = 0; dimIdx < dimWorld; ++dimIdx)
                {
                    if (dimIdx == couplingPlaneDirectionIdx)
                        continue;

                    if (considerOuterSides)
                        isCoupledFace = abs(scvf.center()[dimIdx] - lowDimPos[dimIdx]) < cutThroatRadius + eps;
                    else
                        isCoupledFace = abs(scvf.center()[dimIdx] - lowDimPos[dimIdx]) < abs(fvGeometry.scv(scvf.insideScvIdx()).center()[dimIdx] - lowDimPos[dimIdx]);
                }

                if (isCoupledFace)
                    result.coupledNormalFaces.push_back(scvf.dofIndex());
            }
        }
        return result;
    }

    //! find all bulk elements that are coupled to a lowDim dof (those within the projected throat radius at the interface)
    std::vector<std::size_t> getAllCoupledElements_(const CouplingManager& couplingManager,
                                                    const Element<bulkIdx>& directlyCoupledElement,
                                                    const GlobalPosition& lowDimPos,
                                                    const Scalar throatRadius,
                                                    const int coupledDirectionIndex) const
    {
        std::vector<std::size_t> allElementIndices;

        const auto& bulkFvGridGeometry = couplingManager.problem(bulkIdx).fvGridGeometry();

        auto isWithinThroatRadius = [coupledDirectionIndex, throatRadius](const GlobalPosition& distance)
        {
            std::array<bool, dimWorld> result;
            std::fill(result.begin(), result.end(), false);

            using std::abs;
            for(int i = 0; i < dimWorld; ++i)
             if(abs(distance[i]) < throatRadius)
                 result[i] = true;

            return std::all_of(result.begin(), result.end(), [](auto x){ return x == true; });
        };

        std::stack<Element<bulkIdx>> elementStack;
        elementStack.push(directlyCoupledElement);
        allElementIndices.push_back(bulkFvGridGeometry.elementMapper().index(directlyCoupledElement));

        while (!elementStack.empty())
        {
            auto e = elementStack.top();
            elementStack.pop();
            for (const auto& intersection : intersections(bulkFvGridGeometry.gridView(), e))
            {
                if (intersection.neighbor())
                {
                    const auto outsideElement = intersection.outside();
                    auto bulkFvGeometry = localView(bulkFvGridGeometry);
                    bulkFvGeometry.bind(outsideElement);
                    const auto bulkNeighborElemIdx = bulkFvGridGeometry.elementMapper().index(outsideElement);

                    // skip if the neighboring element at hand was already added
                    if(std::find(allElementIndices.begin(), allElementIndices.end(), bulkNeighborElemIdx) != allElementIndices.end())
                        continue;

                    for(auto&& scvf : scvfs(bulkFvGeometry))
                    {
                     const auto delta = scvf.center() - lowDimPos;
                     using std::abs;
                     const Scalar eps = scvf.area()*1e-8;
                     if(abs(delta[coupledDirectionIndex]) < eps)
                         if(isWithinThroatRadius(delta))
                         {
                             allElementIndices.push_back(bulkNeighborElemIdx);
                             elementStack.push(outsideElement);
                         }
                    }
                }
            }
        }

        std::sort(allElementIndices.begin(), allElementIndices.end());
        allElementIndices.erase(std::unique(allElementIndices.begin(), allElementIndices.end()), allElementIndices.end());

        return allElementIndices;
    }
};

} // end namespace Dumux

#endif
