// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \copydoc Dumux::SnappyGridCreator
 */
#ifndef DUMUX_SNAPPY_GRID_CREATOR_HH
#define DUMUX_SNAPPY_GRID_CREATOR_HH

#include <dumux/common/properties.hh>
#include <dune/grid/io/file/dgfparser/dgfparser.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dumux/common/geometry/intersectspointgeometry.hh>
#include <dumux/common/geometry/makegeometry.hh>
#include <dumux/porenetworkflow/common/geometry.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dune/geometry/affinegeometry.hh>
#include <dune/geometry/referenceelements.hh>

namespace Dumux
{

template<class OtherGridCreator>
class SnappyGridCreatorHelper
{
    using OtherGrid = typename OtherGridCreator::Grid;
    using Scalar = typename OtherGrid::ctype;

    static constexpr auto dim = OtherGrid::dimension;
    static constexpr auto dimWorld = OtherGrid::dimensionworld;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    static constexpr auto planeDim = dimWorld - 1;
    using PlaneGeometryType = Dune::AffineGeometry< Scalar, planeDim, dimWorld >;

public:

    /*!
     * \brief Creates a geometrical plane object.
     *        This is a specialization for 2D, therefore the plane is actually a line.
     *
     * \param p0 The first corner
     * \param p1 The second corner
     */
    static PlaneGeometryType makePlane(const GlobalPosition& p0, const GlobalPosition& p1)
    {
        const std::vector< Dune::FieldVector< Scalar, dimWorld > > corners = {p0, p1};
        return PlaneGeometryType(Dune::GeometryTypes::line, corners);
    }

    /*!
     * \brief Creates a geometrical plane object.
     *        This is a specialization for 3D.
     *
     * \param p0 The first corner
     * \param p1 The second corner
     * \param p2 The third corner
     * \param p3 The fourth corner
     */
    static PlaneGeometryType makePlane(const GlobalPosition& p0,
                                       const GlobalPosition& p1,
                                       const GlobalPosition& p2,
                                       const GlobalPosition& p3)
    {
        const std::vector< Dune::FieldVector< Scalar, dimWorld > > corners = {p0, p1, p2, p3};
        return Dumux::makeDuneQuadrilaterial(corners);
        // return PlaneGeometryType(Dune::GeometryTypes::quadrilateral, corners);
    }

    template<class OtherGridView, class ThroatRadiusHelper>
    static auto getPointsInPlane(const OtherGridView& otherGridView, const ThroatRadiusHelper throatRadiusHelper, const PlaneGeometryType& plane)
    {
        std::vector<bool> vertexVisited(otherGridView.size(dim), false);
        static const GlobalPosition couplingPlaneNormal = getParam<GlobalPosition>("Problem.CouplingPlaneNormal", GlobalPosition{0.0, 1.0});

        struct Data { GlobalPosition pos; Scalar area;};
        std::vector<Data> result;

        for(const auto& element : elements(otherGridView))
        {
            const auto& pos0 = element.geometry().corner(0);
            const auto& pos1 = element.geometry().corner(1);

            if(intersectsPointGeometry(pos0, plane) || intersectsPointGeometry(pos1, plane))
            {
                GlobalPosition pointOnTop;
                GlobalPosition otherPoint;
                int vIdx = -1;
                if(intersectsPointGeometry(pos0, plane))
                {
                    pointOnTop = pos0;
                    otherPoint = pos1;
                    vIdx = otherGridView.indexSet().subIndex(element, 0, dim);
                }
                if(intersectsPointGeometry(pos1, plane))
                {
                    pointOnTop = pos1;
                    otherPoint = pos0;
                    vIdx = otherGridView.indexSet().subIndex(element, 1, dim);
                }

                if(vertexVisited[vIdx])
                    continue;
                else
                    vertexVisited[vIdx] = true;

                const Scalar throatRadius = throatRadiusHelper(element);
                const Scalar cutThroatRadius = projectedThroatRadius(throatRadius, element, couplingPlaneNormal);

                result.push_back({pointOnTop, 2*cutThroatRadius});
            }

            std::sort(result.begin(), result.end(),
                      [](const auto& pore0, const auto& pore1)
                      {
                          return pore0.pos[0] < pore1.pos[0];
                      });

        }
        return result;
    }
};


/*!
 * \brief  A grid creator that matches a bulk Stokes grid to a PNM grid.
 */
template<int dim, class OtherGridCreator, decltype(DiscretizationMethod::none) DiscMethod = DiscretizationMethod::none>
class SnappyGridCreator : public GridManager< Dune::YaspGrid<dim, Dune::TensorProductCoordinates<typename OtherGridCreator::Grid::ctype, OtherGridCreator::Grid::LeafGridView::dimensionworld> >>
{
    using Scalar = typename OtherGridCreator::Grid::ctype;
    using OtherGrid = typename OtherGridCreator::Grid;
    using IntVector = std::vector<int>;
    using ScalarVector = std::vector<Scalar>;

    static constexpr auto dimWorld = OtherGrid::LeafGridView::dimensionworld;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using ParentType = GridManager< Dune::YaspGrid<dim, Dune::TensorProductCoordinates<typename OtherGridCreator::Grid::ctype, OtherGridCreator::Grid::LeafGridView::dimensionworld> >>;
    using OtherGridData = typename OtherGridCreator::GridData;

public:
    using Grid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;

    using ParentType::ParentType;

    //! make the grid
    void init(const OtherGrid& otherGrid, const OtherGridData& otherData, const std::string& modelParamGroup = "")
    {
        // we just forward to the other method and discard the temporary vector afterwards
        ScalarVector tmp;
        init(otherGrid, otherData, tmp, modelParamGroup);
    }

    //! make the grid and store the locations the grid is adapted to (i.e. the pore throats intersecting the interface)
    void init(const OtherGrid& otherGrid, const OtherGridData& otherData, ScalarVector& auxiliaryPositions, const std::string& modelParamGroup = "")
    {
        std::array<ScalarVector, dim> positions;
        std::array<IntVector, dim> cells;
        std::array<ScalarVector, dim> grading;

        const auto lowerLeft = getParamFromGroup<GlobalPosition>(modelParamGroup, "Grid.LowerLeft");
        const auto upperRight = getParamFromGroup<GlobalPosition>(modelParamGroup, "Grid.UpperRight");
        using SnappyGridCreatorHelper = Dumux::SnappyGridCreatorHelper<OtherGridCreator>;

        auto throatRadiusHelper = [&](const auto& element)
        {
            return otherData.parameters(element)[otherData.index(OtherGridData::ElementParameter::radius)];
        };

        // search for the intersection pore bodies
        const auto plane = SnappyGridCreatorHelper::makePlane(lowerLeft, GlobalPosition{upperRight[0], lowerLeft[1]});
        const auto points = SnappyGridCreatorHelper::getPointsInPlane(otherGrid.leafGridView(), throatRadiusHelper, plane);

        // set the positions in x-direction
        // set the leftmost point
        positions[0].push_back(lowerLeft[0]);

        // check for user-defined addtional points in the inlet
        const ScalarVector inletPositions = getParamFromGroup<ScalarVector>(modelParamGroup, "Grid.InletPositions", ScalarVector{});
        if(!inletPositions.empty())
        {
            for(auto&& pos : inletPositions)
            {
                if((pos < points[0].pos[0] - points[0].area/2) && (pos > lowerLeft[0]))
                    positions[0].push_back(pos);
                else
                    DUNE_THROW(Dune::RangeError, "Make sure to set positions only in the inlet");
            }
        }

        // set the points for the pore body positions
        for(const auto& point : points)
        {
            const auto left = point.pos[0] - point.area/2;
            const auto right = point.pos[0] + point.area/2;
            if(left <= positions[0].back())
                DUNE_THROW(Dune::RangeError, "Throat radii are too large, they intersect!");

            positions[0].push_back(left);
            positions[0].push_back(right);
            auxiliaryPositions.push_back(left);
            auxiliaryPositions.push_back(right);
        }

        // check for user-defined addtional points in the outlet
        const ScalarVector outletPositions = getParamFromGroup<ScalarVector>(modelParamGroup, "Grid.OutletPositions", ScalarVector{});
        if(!outletPositions.empty())
        {
            for(auto&& pos : outletPositions)
            {
                if((pos > positions[0].back()) && (pos < upperRight[0]))
                    positions[0].push_back(pos);
                else
                    DUNE_THROW(Dune::RangeError, "Make sure to set ascending positions only in the outlet");
            }
        }

        // set the rightmost point
        positions[0].push_back(upperRight[0]);

        // set the positions in y-direction
        // set the lowest point
        positions[1].push_back(lowerLeft[1]);

        // check for user-defined additional points in y direction
        const ScalarVector positionsY = getParamFromGroup<ScalarVector>(modelParamGroup, "Grid.PositionsY", ScalarVector{});
        if(!positionsY.empty())
        {
            for(auto&& pos : positionsY)
            {
                if((pos > positions[1].back()) && (pos < upperRight[1]))
                    positions[1].push_back(pos);
                else
                    DUNE_THROW(Dune::RangeError, "Make sure to set ascending y-positions only within the channel");
            }
        }

        // set the point at the top
        positions[1].push_back(upperRight[1]);

        // set the number of cells in the inlet
        if(inletPositions.empty())
        {
            const int cellsInletX = getParamFromGroup<int>(modelParamGroup, "Grid.CellsInletX");
            cells[0].push_back(cellsInletX);
        }
        else
        {
            IntVector cellsInletX = getParamFromGroup<IntVector>(modelParamGroup, "Grid.CellsInletX");

            if(cellsInletX.size() != inletPositions.size() + 1)
                DUNE_THROW(Dune::RangeError, "CellsInletX must equal InletPositions + 1");

            for(auto&& i : cellsInletX)
                cells[0].push_back(i);
        }

        // set the number of cells above the porous medium
        const int cellsPerThroat = getParamFromGroup<int>(modelParamGroup, "Grid.CellsPerThroat");
        if(cellsPerThroat % 2 == 0)
            DUNE_THROW(Dune::RangeError, "Number of cells per throat must be odd!");

        for(int i = 0; i < points.size(); ++i)
        {
            cells[0].push_back(cellsPerThroat);
            if(i < points.size() -1)
            {
                const auto spacingLeft = points[i].area / cellsPerThroat;
                const auto spacingRight = points[i+1].area / cellsPerThroat;
                const auto avgSpacing = (spacingLeft + spacingRight) / 2;
                const auto lengthBetween = (points[i+1].pos[0] - (points[i+1].area / 2))
                                         - (points[i].pos[0] + (points[i].area / 2));
                const int cellsBetween = std::ceil(lengthBetween / avgSpacing);
                cells[0].push_back(cellsBetween);
            }
        }

        // set the number of cells in the outlet
        if(outletPositions.empty())
        {
            const int cellsOutletX = getParamFromGroup<int>(modelParamGroup, "Grid.CellsOutletX");
            cells[0].push_back(cellsOutletX);
        }
        else
        {
            const IntVector cellsOutletX = getParamFromGroup<IntVector>(modelParamGroup, "Grid.CellsOutletX");

            if(cellsOutletX.size() != outletPositions.size() + 1)
                DUNE_THROW(Dune::RangeError, "CellsOutletX must equal OutletPositions + 1");

            for(auto&& i : cellsOutletX)
                cells[0].push_back(i);

        }

        // set the number of cells in y-direction
        if(positionsY.empty())
        {
            const int cellsY = getParamFromGroup<int>(modelParamGroup, "Grid.CellsY");
            cells[1].resize(positions[1].size()-1, cellsY);
        }
        else
        {
            const IntVector cellsY = getParamFromGroup<IntVector>(modelParamGroup, "Grid.CellsY");

            if(cellsY.size() != positionsY.size() + 1)
                DUNE_THROW(Dune::RangeError, "CellsY must equal PositionsY + 1");

            for(auto&& i : cellsY)
                cells[1].push_back(i);
        }


        // grading factor (has a default)
        grading[0].resize(positions[0].size()-1, 1.0);
        grading[1].resize(positions[1].size()-1, 1.0);

        // inlet grading
        if(!inletPositions.empty())
        {
            const ScalarVector inletGrading = getParamFromGroup<ScalarVector>(modelParamGroup, "Grid.InletGrading", ScalarVector{});
            if(!inletGrading.empty())
            {
                if(inletGrading.size() != inletPositions.size() + 1)
                    DUNE_THROW(Dune::RangeError, "InletGrading must equal InletPositions + 1");

                for(int i = 0; i < inletPositions.size(); ++i)
                    grading[0][i] = inletGrading[i];
            }
        }

        // outlet grading
        if(!outletPositions.empty())
        {
            const ScalarVector outletGrading = getParamFromGroup<ScalarVector>(modelParamGroup, "Grid.OutletGrading", ScalarVector{});
            if(!outletGrading.empty())
            {
                if(outletGrading.size() != outletPositions.size() + 1)
                    DUNE_THROW(Dune::RangeError, "OutletGrading must equal OutletPositions + 1");

                const int offSet = positions[0].size() - outletPositions.size() - 2;
                for(int i = 0; i < outletPositions.size() + 1; ++i)
                    grading[0][offSet + i] = outletGrading[i];
            }
        }

        // grading in y-direction
        if(!positionsY.empty())
        {
            const ScalarVector gradingY = getParamFromGroup<ScalarVector>(modelParamGroup, "Grid.GradingY", ScalarVector{});
            if(!gradingY.empty())
            {
                if(gradingY.size() != positionsY.size() + 1)
                    DUNE_THROW(Dune::RangeError, "GradingY must equal PositionsY + 1");

                for(int i = 0; i < positionsY.size() + 1; ++i)
                    grading[1][i] = gradingY[i];
            }
        }

        // forward to the actual grid creator
        ParentType::init(positions, cells, grading);
    }

};
}

#endif
