// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters for pore network models.
 */
#ifndef DUMUX_PNM_RANDOM_NETWORK_SPATIAL_PARAMS_1P_HH
#define DUMUX_PNM_RANDOM_NETWORK_SPATIAL_PARAMS_1P_HH

#include <dumux/porenetworkflow/common/geometry.hh>
#include <dumux/material/spatialparams/porenetwork/porenetwork1p.hh>

namespace Dumux
{


/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class FVGridGeometry, class Scalar, class Labels, Shape poreGeometry, Shape throatGeometry>
class RandomNetWorkSpatialParams : public PNMOnePSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry>
{
    using ParentType = PNMOnePSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;

public:
    using PermeabilityType = Scalar;
    using ParentType::ParentType;

    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    template<class VolumeVariables>
    Scalar poreVolume(const SubControlVolume &scv, const VolumeVariables& volVars) const
    {
        return initialPoreVolume(scv.dofIndex());
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    Scalar poreVolume(const SubControlVolume &scv) const
    {
        return initialPoreVolume(scv.dofIndex());
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    template<class VolumeVariables>
    Scalar poreVolume(const int dofIdxGlobal, const VolumeVariables& volVars) const
    {
        return initialPoreVolume(dofIdxGlobal);
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    Scalar poreVolume(const int dofIdxGlobal) const
    {
        return initialPoreVolume(dofIdxGlobal);
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    Scalar initialPoreVolume(const SubControlVolume& scv) const
    {
        const auto yMax = this->fvGridGeometry().bBoxMax()[1];
        if (scv.dofPosition()[1] > yMax - 1e-10)
            return 0.5*initialPoreVolume(scv.dofIndex());
        else
            return initialPoreVolume(scv.dofIndex());
    }


    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    Scalar initialPoreVolume(const int dofIdxGlobal) const
    {
        return M_PI*this->poreRadius(dofIdxGlobal)*this->poreRadius(dofIdxGlobal);
    }



};

} // namespace Dumux

#endif
