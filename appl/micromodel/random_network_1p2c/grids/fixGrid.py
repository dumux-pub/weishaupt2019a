import numpy as np


with open("voronoi_76.dgf") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

startIdxVertices = 0
endIdxVertices = 0
startIdxElement = 0
endIdxElement = 0
endCounter = 0

print content[0]

for idx, line in enumerate(content):
    if line.startswith('Vertex %'):
        startIdxVertices = idx + 2
    if line.startswith('SIMPLEX %'):
        endIdxVertices = idx - 2
        startIdxElement = idx + 2
    if line.startswith('#'):
        endCounter += 1
        if endCounter == 2:
            endIdxElement = idx - 1

print startIdxVertices, endIdxVertices, startIdxElement, endIdxElement



vertices = [np.fromstring(line, dtype=float, sep=' ') for line in content[startIdxVertices:endIdxVertices+1]]
elements = [np.fromstring(line, dtype=float, sep=' ') for line in content[startIdxElement:endIdxElement+1]]




bBoxMax = [-1e9, -1e9]

for vertex in vertices:
    bBoxMax[0] = max(bBoxMax[0], vertex[0])
    bBoxMax[1] = max(bBoxMax[1], vertex[1])

print bBoxMax

print vertices[0][1]

print "vertices : " ,len(vertices)

# print int(elements[0][1])

vertexChanged = [False]*len(vertices)

origPoreRadii = [x[2] for x in vertices]

def projectedThroatRadius(element):
    vIdx0 = int(element[0])
    vIdx1 = int(element[1])
    vertex0 = vertices[vIdx0]
    vertex1 = vertices[vIdx1]
    throatOrientationVector = np.array([vertex1[0], vertex1[1]]) - np.array([vertex0[0], vertex0[1]])
    cutPlaneNormal = np.array([0, 1])

    # get the angle between the throat and the plane it cuts
    cosPhi = np.dot(cutPlaneNormal, throatOrientationVector) / (np.linalg.norm(cutPlaneNormal) * np.linalg.norm(throatOrientationVector));
    phi = np.arccos(cosPhi) # in radians
    alpha = phi - 0.5*np.pi if phi > 0.5*np.pi else 0.5*np.pi - phi # 0.5*pi rad == 90 deg; we define alpha always smaller than 90 deg

    #get the radius projected onto the plane the throat cuts
    return element[2] / np.sin(alpha);

for element in elements:
    vIdx0 = int(element[0])
    vIdx1 = int(element[1])

    projectedThroatRadius(element)

    if vertices[vIdx0][1] > bBoxMax[1] - 1e-8:
        vertices[vIdx0][2] = projectedThroatRadius(element)#element[2]
        vertexChanged[vIdx0] = True

    if vertices[vIdx1][1] > bBoxMax[1] - 1e-8:
        vertices[vIdx1][2] = projectedThroatRadius(element)#element[2]
        vertexChanged[vIdx1] = True

# Write the actual DGF file (cleaned-up)
with open("new" + '.dgf', 'w') as gridfile:
    gridfile.write('DGF\nVertex % Coordinates and volumes of the pore bodies\n')
    gridfile.write('parameters 2\n')

    for vertex in vertices:
        gridfile.write(str(vertex[0]) + ' ' + str(vertex[1]) + ' ' + str(vertex[2]) + ' ' + str(vertex[3]) + '\n')

    gridfile.write('#\nSIMPLEX % Connections of the pore bodies (pore throats)\n')
    gridfile.write('parameters 2\n')

    # write the elements to the dgf file
    for idx, throat in enumerate(elements):
        # get the vertex indices
        poreIdx0 = int(throat[0])
        poreIdx1 = int(throat[1])

        throatLength = throat[3]

        if vertexChanged[poreIdx0]:
            throatLength += origPoreRadii[poreIdx0] - vertices[poreIdx0][2]
        if vertexChanged[poreIdx1]:
            throatLength += origPoreRadii[poreIdx1] - vertices[poreIdx1][2]

        # write to the dgf file
        gridfile.write(str(poreIdx0) + ' ' + str(poreIdx1) + ' ' + str(throat[2]) + ' ' + str(throatLength)  + '\n')

    gridfile.write('#')
