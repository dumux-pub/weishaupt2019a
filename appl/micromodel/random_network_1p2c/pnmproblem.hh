// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P_PROBLEM_HH
#define DUMUX_PNM1P_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/1pnc/model.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase2c.hh>

namespace Dumux
{
template <class TypeTag>
class PNMOnePProblem;

namespace Properties
{
NEW_TYPE_TAG(PNMOnePTypeTag, INHERITS_FROM(PNMOnePNC));

// Set the problem property
SET_TYPE_PROP(PNMOnePTypeTag, Problem, Dumux::PNMOnePProblem<TypeTag>);

// the fluid system
SET_PROP(PNMOnePTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using H2O = Components::SimpleH2O<Scalar>;
    using Tracer = Components::Constant<1, Scalar>;
public:
  using type = FluidSystems::LiquidPhaseTwoC<Scalar, H2O, Tracer>;
};

// Set the grid type
SET_TYPE_PROP(PNMOnePTypeTag, Grid, Dune::FoamGrid<1, 2>);

SET_TYPE_PROP(PNMOnePTypeTag, SinglePhaseTransmissibility, TransmissibilityBruus<TypeTag, false>);

SET_PROP(PNMOnePTypeTag, ThroatGeometry)
{
    static const Shape value = Shape::TwoPlates;
};

SET_PROP(PNMOnePTypeTag, PoreGeometry)
{
    static const Shape value = Shape::TwoPlates;
};

SET_BOOL_PROP(PNMOnePTypeTag, UseMoles, true);

SET_INT_PROP(PNMOnePTypeTag, ReplaceCompEqIdx, 3);

SET_PROP(PNMOnePTypeTag, SpatialParams)
{
private:
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
    static constexpr auto poreGeometry = GET_PROP_VALUE(TypeTag, PoreGeometry);
    static constexpr auto throatGeometry = GET_PROP_VALUE(TypeTag, ThroatGeometry);
public:
    using type = RandomNetWorkSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry>;
};

}

template <class TypeTag>
class PNMOnePProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<GridView::dimension>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);

public:
    template<class SpatialParams>
    PNMOnePProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<SpatialParams> spatialParams,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, spatialParams, "PNM"), eps_(1e-7), couplingManager_(couplingManager)
    {
        const Scalar deltaP = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaP");
        bottomPressure_ = 1e5 + deltaP;
        bottomMoleFraction_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.BottomMoleFraction", 0.0);
        closedBottom_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.ClosedBottom");
    }


    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; }

     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        if (couplingManager().isCoupledDof(CouplingManager::lowDimIdx, scv.dofIndex()))
            bcTypes.setAllCouplingNeumann();
        else if (!closedBottom_ && this->spatialParams().poreLabel(scv.dofIndex()) == 2)
            bcTypes.setAllDirichlet();
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();

        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);

        values[Indices::pressureIdx] = bottomPressure_;
        values[Indices::conti0EqIdx + 1] = bottomMoleFraction_;
        return values;
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{


    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    template<class ElementVolumeVariables>
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);

        const int vIdx =  scv.dofIndex();

        if (couplingManager().isCoupledDof(CouplingManager::lowDimIdx, vIdx))
            values = couplingManager().couplingData().massCouplingCondition(scv, elemVolVars[scv]);

        values /= this->spatialParams().initialPoreVolume(scv);

        return values;
    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);
            values[Indices::pressureIdx] = 1e5;
#if NONISOTHERMAL
            values[Indices::temperatureIdx] = 273.15 +20;
#endif
            return values;
    }

    // \}

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    Scalar eps_;
    std::shared_ptr<CouplingManager> couplingManager_;
    Scalar bottomPressure_;
    Scalar bottomMoleFraction_;
    bool closedBottom_;

};
} //end namespace

#endif
