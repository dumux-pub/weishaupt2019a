#!/usr/bin/env python

import matplotlib
import matplotlib.pyplot as plt
from functions import *
import glob


def graph(directory):
    # matplotlib.rc('text', usetex = True)

    # plt.style.use('ggplot')
    # plt.style.use('grayscale')

    # get all files in the given directory
    filenames = sorted(glob.glob(directory + '*log'))

    filenamesPNM = sorted(glob.glob(directory + '*csv'))
    # filenames = sorted(glob.glob('./log_250_lr/*log'))
    # filenames = sorted(glob.glob('./log_250_dia/*log'))
    # filenames = sorted(glob.glob('./log_100_lr/*log'))
    # filenames = sorted(glob.glob('./log_100_dia/*log'))
    # filenames = sorted(glob.glob('./log_50_dia/*log'))
    # filenames = sorted(glob.glob('./log_50_lr/*log'))

    # expects a filename like lr_250_40.log

    print filenames

    # extract the fluxes and the file names
    fluxes = [ getPlaneFluxes(x) for x in filenames]
    names = [ name.split('/')[-1].split('.')[0] for name in filenames]

    # set the main title
    title = "Convergence \nthroat width = " + names[0].split('_')[1] + " $\mathsf{\mu m}$"
    if names[0].split('_')[0] == "lr":
        title += ", left to right flow"
    else:
        title += ", diagonal flow"

    def numThroats(idx):
        return int(names[idx].split('_')[-1])

    # create a dictionary for convenience
    data = {key: value for (key, value) in zip(names, fluxes)}

    # create a figure (2x2)
    fig = plt.figure(figsize=(10,10))
    fig.suptitle(title, weight='bold', fontsize=16)

    sub1 = fig.add_subplot(2,2,1)
    sub2 = fig.add_subplot(2,2,2)
    sub3 = fig.add_subplot(2,2,3)
    sub4 = fig.add_subplot(2,2,4)

    # plot the two upper figures
    xUpperLeft = range(len(fluxes[0]['planeSubFluxesY'][9]))
    xUpperRight = range(len(fluxes[0]['planeSubFluxesX'][4]))
    labels = [int(i.split('_')[-1]) for i in data]
    for idx, val in enumerate(fluxes):
        sub1.plot(val['planeSubFluxesX'][9], xUpperLeft, 'o', label=numThroats(idx))
        # sub1.plot(xUpperLeft, val['planeSubFluxesX'][9], 'o', label=numThroats(idx))
        sub2.plot(xUpperRight, val['planeSubFluxesY'][9], 'o', label=numThroats(idx))

    # upper left figure
    # add PNM data
    pnmHorizontalFlux = pnmData(filenamesPNM)['right']
    sub1.plot(pnmHorizontalFlux, xUpperLeft, 'x', label='PNM')

    sub1.set_title('Horizontal fluxes, \nrightmost row of throats\n')
    sub1.legend(prop={'size':8}, ncol= 2,numpoints=1, title=' cells per throat width')
    sub1.set_ylabel('throat')
    sub1.set_xlabel('specific flux per throat ['r'$\mathsf{m^3/(m s)}}$]')
    # sub1.set_xlim([0.5, 8.5])
    # sub1.set_ylim([3.3e-10,4.2e-10])

    # upper right figure
    # add PNM data
    pnmVerticalFlux = pnmData(filenamesPNM)['top']
    pnmVerticalFlux = [-x for x in pnmVerticalFlux]
    sub2.plot(xUpperRight, pnmVerticalFlux, 'x', label='PNM')

    sub2.set_title('Vertical fluxes, \ntop\n')
    sub2.legend(loc='lower right',prop={'size':8}, ncol= 2,numpoints=1, title=' of cells per throat width')
    sub2.set_xlabel('throat')
    sub2.set_ylabel('specific flux per throat ['r'$\mathsf{m^3/(m s)}}$]')


    # lower left figure
    resultsThroatXUpperRight = [x['planeSubFluxesX'][9][8] for x in fluxes]
    x3 = [numThroats(idx) for idx in range(len(fluxes))]

    sub3.plot(x3, resultsThroatXUpperRight, 'o-', label='hor. throat upper right')
    # sub3.set_ylim([3.3e-10,4.2e-10])
    # sub3.set_xlim([5,80])
    # sub3.legend(loc='center right',prop={'size':8}, ncol= 3,numpoints=1)
    sub3.set_xlabel('cells per throat width')
    sub3.set_ylabel('specific flux per throat ['r'$\mathsf{m^3/(m s)}}$]')
    sub3.set_title('horizontal flux, \nrightmost upper throat\n')
    # sub3.set_xticks([5, 10, 20, 40, 80])
    sub3.set_xticks(x3)
    # for tick in sub3.get_yticklabels():
    #     tick.set_rotation(90)


    # lower right figure
    diff = [computeDifference(x['planeSubFluxesX'], fluxes[-1]['planeSubFluxesX']) for x in fluxes]
    L2norm = [x['L2norm'] for x in diff]
    x4 = x3

    # remove the last entry as it compares the fine solution with itself
    L2norm = L2norm[:-1]
    x4 = x4[:-1]

    sub4.plot(x4, L2norm, 'o-')
    sub4.set_yscale("log")
    sub4.set_title('Horizontal flux, \nglobal L2-Norm\n')
    sub4.set_xlabel('cells per throat width')
    sub4.set_ylabel('L2-error \ncompared to 80 \nthroats p. len.')


    fig.tight_layout()
    fig.subplots_adjust(top=0.85)

    fig.savefig('./convergence_' + names[0].split('_')[0] + '_' +names[0].split('_')[1] + '.pdf', bbox_inches='tight')

graph('./log_50_lr/')
graph('./log_50_dia/')
graph('./log_100_lr/')
graph('./log_100_dia/')
graph('./log_250_lr/')
graph('./log_250_dia/')
