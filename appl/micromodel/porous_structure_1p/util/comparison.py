#!/usr/bin/env python

import matplotlib
import matplotlib.pyplot as plt
from functions import *
import glob
import numpy as np



def graphLeftToRight():

    bar_width = {'250':1.0, '100':1, '50':1}
    fig = plt.figure(figsize=(5,3))

    for figIdx, size in enumerate(['50','100','250']):

        # get all files in the given directory
        filenamesRef = sorted(glob.glob('./log_' + size + '_lr/*log'))
        filenamesPNM = sorted(glob.glob('./log_' + size + '_lr/*csv'))

        # extract the fluxes of the lower right horizontal throat
        fluxRef = sum(getPlaneFluxes(filenamesRef[-1])['planeSubFluxesX'][9])
        fluxPNM = sum(pnmData(filenamesPNM)['right'])

        # sub1 = fig.add_subplot(3,1,figIdx+1)
        sub1 = fig.add_subplot(1,1,1)
        # sub1.spines["top"].set_visible(False)
        # sub1.spines["left"].set_visible(False)
        # sub1.spines["right"].set_visible(False)
        # sub1.spines["bottom"].set_visible(False)

        labelRef = "reference" if figIdx == 0 else ""
        labelPNM = "PNM" if figIdx == 0 else ""
        sub1.barh(figIdx*3*bar_width[size] - 0.5*bar_width[size], fluxRef, bar_width[size], log=True, color=tableau20[0], alpha=1, label=labelRef)
        sub1.barh(figIdx*3*bar_width[size] + 0.5*bar_width[size], fluxPNM, bar_width[size], log=True, color=tableau20[2], alpha=1, label=labelPNM)

        plt.xscale('log', nonposy='clip')
        sub1.set_xlim([10e-12,5*10e-9])


        sub1.set_yticks([0,3,6])
        sub1.set_ylabel('throat diameter [$\mathsf{\mu}$m]')
        sub1.set_yticklabels(['50', '100', '250'])
        # sub1.yaxis.set_visible(False)
        # sub1.xaxis.set_visible(False)

        percentualDiff = (fluxPNM - fluxRef)/fluxRef * 100.0

        sub1.text(1.1*fluxPNM, figIdx*3*bar_width[size], '{:+.2f}'.format(percentualDiff)+" %", color='black')
        sub1.set_xlabel('cumulative horizontal flux ['r'$\mathsf{m^3/(m s)}}$]')
        # sub1.legend(bbox_to_anchor=(0.7, 8.5), ncol=2)
        sub1.legend(ncol=2)

    # sub1.legend(loc='center',prop={'size':8}, ncol= 2,numpoints=1)
    fig.tight_layout()
    fig.subplots_adjust(top=0.85)
    fig.savefig("leftToRight_hor.pdf")

def graphDiagonal():

    bar_width = {'250':1.0, '100':1, '50':1}
    fig1 = plt.figure(figsize=(5,3))
    fig2 = plt.figure(figsize=(5,3))

    for figIdx, size in enumerate(['50','100','250']):

        # get all files in the given directory
        filenamesRef = sorted(glob.glob('./log_' + size + '_dia/*log'))
        filenamesPNM = sorted(glob.glob('./log_' + size + '_dia/*csv'))

        # extract the fluxes of the lower right horizontal throat
        fluxRef = {}
        fluxPNM = {}
        fluxRef['horizontal'] = sum(getPlaneFluxes(filenamesRef[-1])['planeSubFluxesX'][9])
        fluxPNM['horizontal'] = sum(pnmData(filenamesPNM)['right'])
        fluxRef['vertical'] = abs(sum(getPlaneFluxes(filenamesRef[-1])['planeSubFluxesY'][9]))
        fluxPNM['vertical'] = sum(pnmData(filenamesPNM)['top'])

        # calculate some percentual differences between PNM and reference
        percentualDiff = {}
        percentualDiff['horizontal'] = (fluxPNM['horizontal'] - fluxRef['horizontal'])/fluxRef['horizontal'] * 100.0
        percentualDiff['vertical'] = (fluxPNM['vertical'] - fluxRef['vertical'])/fluxRef['vertical'] * 100.0

        sub1 = fig1.add_subplot(1,1,1)
        sub2 = fig2.add_subplot(1,1,1)

        labelRef = "reference" if figIdx == 0 else ""
        labelPNM = "PNM" if figIdx == 0 else ""

        # the horizontal fluxes
        sub1.barh(figIdx*3*bar_width[size] - 0.5*bar_width[size], fluxRef['horizontal'], bar_width[size], log=True, color=tableau20[0], alpha=1, label=labelRef)
        sub1.barh(figIdx*3*bar_width[size] + 0.5*bar_width[size], fluxPNM['horizontal'], bar_width[size], log=True, color=tableau20[2], alpha=1, label=labelPNM)

        sub1.set_xscale('log')
        sub1.set_xlim([10e-12,5*10e-9])

        sub1.set_yticks([0,3,6])
        sub1.set_ylabel('throat diameter [$\mathsf{\mu}$m]')
        sub1.set_yticklabels(['50', '100', '250'])
        sub1.set_xlabel('cumulative horizontal flux ['r'$\mathsf{m^3/(m s)}}$]')

        # sub1.yaxis.set_visible(False)
        # sub1.xaxis.set_visible(False)

        sub1.text(1.1*fluxPNM['horizontal'], figIdx*3*bar_width[size], '{:+.2f}'.format(percentualDiff['horizontal'])+" %", color='black')
        sub1.legend(ncol=2)
        # sub1.legend(bbox_to_anchor=(0.7, 8.5), ncol=2)


        #the vertical fluxes
        sub2.bar(figIdx*3*bar_width[size] - 0.5*bar_width[size], fluxRef['vertical'], bar_width[size], log=True, color=tableau20[0], alpha=1, label=labelRef)
        sub2.bar(figIdx*3*bar_width[size] + 0.5*bar_width[size], fluxPNM['vertical'], bar_width[size], log=True, color=tableau20[2], alpha=1, label=labelPNM)

        sub2.set_yscale('log')
        sub2.set_ylim([5*10e-13,1*10e-9])

        sub2.set_xticks([0,3,6])
        sub2.set_xlabel('throat diameter [$\mathsf{\mu}$m]')
        sub2.set_xticklabels(['50', '100', '250'])
        sub2.legend(ncol=2)

        sub2.set_ylabel('cumulative vertical flux ['r'$\mathsf{m^3/(m s)}}$]')
        sub2.text(figIdx*3*bar_width[size] - 0.5*bar_width[size], 1.2*fluxPNM['vertical'], '{:+.2f}'.format(percentualDiff['vertical'])+" %", color='black')

    fig1.tight_layout()
    fig1.subplots_adjust(top=0.85)
    fig1.savefig("diagonal_hor.pdf")

    fig2.tight_layout()
    fig2.subplots_adjust(top=0.85)
    fig2.savefig("diagonal_vert.pdf")


graphLeftToRight()
graphDiagonal()
