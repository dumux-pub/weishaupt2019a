#!/usr/bin/env python

import re
import numpy as np
import csv


# These are the "Tableau 20" colors as RGB.
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]

# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
    r, g, b = tableau20[i]
    tableau20[i] = (r / 255., g / 255., b / 255.)

def readFile(fileName):
    with open(fileName) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    return content


def getPlaneFluxes(fileName) :
    subFluxes = []
    planeFluxesX = []
    planeFluxesY = []
    planeSubFluxesX = []
    planeSubFluxesY = []

    content = readFile(fileName)

    indexHorizontal = content.index('Horizontal Fluxes')
    indexVertical = content.index('Vertical Fluxes')

    # get index for end of information
    for index, value in enumerate(content):
        if value.startswith("NumCells per throat"):
            indexEnd = index
            break

    planeCounter = -1

    #iterate over the horizontal fluxes
    for line in content[indexHorizontal:indexVertical]:
        if line.startswith('at plane'):
            planeFluxesX.append(float(line.split()[-1]))
            planeCounter = int(re.sub(':', '', line.split()[2]))
            # print planeCounter
            planeSubFluxesX.append([])
        if re.match('[0-9]:+', line):
            planeSubFluxesX[planeCounter].append(float(line.split()[-1]))

    #iterate over the vertical fluxes
    for line in content[indexVertical:indexEnd]:
        if line.startswith('at plane'):
            planeFluxesY.append(float(line.split()[-1]))
            planeCounter = int(re.sub(':', '', line.split()[2]))
            # print planeCounter
            planeSubFluxesY.append([])
        if re.match('[0-9]:+', line):
            planeSubFluxesY[planeCounter].append(float(line.split()[-1]))

    return  {'planeFluxesX':planeFluxesX,
             'planeSubFluxesX':planeSubFluxesX,
             'planeFluxesY':planeFluxesY,
             'planeSubFluxesY':planeSubFluxesY}

def computeDifference(coarse, fine):
    assert len(coarse) == len(fine)

    numDataPoints = 0
    globalDiff = []
    planeIdx = 0

    for (planeDataCoarse,planeDataFine) in zip(coarse, fine):
        planeDataCoarse = np.array(planeDataCoarse, dtype=np.float32)
        planeDataFine = np.array(planeDataFine, dtype=np.float32)

        # print "plane ", planeIdx
        for(valueCoarse, valueFine) in zip(planeDataCoarse, planeDataFine):
            # print valueCoarse, valueFine
            globalDiff.append(valueCoarse - valueFine)

        planeIdx += 1

    globalDiffSquared = [x*x for x in globalDiff]

    result = np.sqrt(sum(globalDiffSquared)) / len(globalDiffSquared)
    return {'diff':globalDiff, 'diffSquared':globalDiffSquared, 'L2norm':result}


def pnmData(filenamesPNM):
    dataPNM = {}
    for filename in filenamesPNM:
        dataName = filename.split('/')[-1].split('_')[-1].split('.')[0]
        dataPNM[dataName] = []
        with open(filename) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            next(readCSV, None) # skip the header
            for row in readCSV:
                dataPNM[dataName].append(float(row[-1]))
    return dataPNM
