// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_STRUCTURED_1P_PROBLEM
#define DUMUX_STRUCTURED_1P_PROBLEM

#include<string>

#include <dune/grid/yaspgrid.hh>
#include <dumux/io/grid/subgridgridcreator.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>
#include <dune/common/float_cmp.hh>
// #include "./../../finetocoarsegridwriter.hh"

namespace Dumux
{
template <class TypeTag>
class StructuredProblem;


namespace Properties
{
NEW_TYPE_TAG(StructuredProblem, INHERITS_FROM(StaggeredFreeFlowModel, NavierStokes));

// the fluid system
SET_PROP(StructuredProblem, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
SET_PROP(StructuredProblem, Grid)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    static constexpr auto dim = 2;

public:
    using HostGrid = Dune::YaspGrid<dim>;
    // using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the problem property
SET_TYPE_PROP(StructuredProblem, Problem, Dumux::StructuredProblem<TypeTag> );

SET_BOOL_PROP(StructuredProblem, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(StructuredProblem, EnableGridFluxVariablesCache, true);
SET_BOOL_PROP(StructuredProblem, EnableGridVolumeVariablesCache, true);
}

/*!
 * \brief  Test problem for the one-phase model:
   \todo doc me!
 */
template <class TypeTag>
class StructuredProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    static constexpr auto dimWorld = GET_PROP_TYPE(TypeTag, GridView)::dimensionworld;

    using Element = typename FVGridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    StructuredProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), eps_(1e-6)
    {
        flowDirection_ = getParam<std::string>("Problem.FlowDirection");
        extrusionFactor_ = getParam<Scalar>("Problem.ExtrusionFactor");
        calculateFluxesOverPlane_ = getParam<bool>("FluxOverPlane.CalculateFluxes");
        pLeft_ = getParam<Scalar>("Problem.PLeft");
    }

    /*!
     * \name Problem parameters
     */
    // \{

    template<class GridVariables, class SolutionVector>
    void getFluxes(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        // if(this->grid().maxLevel() > 0)
        //     FineToCoarseGridWriter::writeFineToCoarseGrid(*this);

        if(!calculateFluxesOverPlane_)
            return;

        // We store the horizontal volume flux in the inlet channel to check whether this fits with the applied BC and in order to
        // calculate a ratio (see below).
        FluxOverSurface<TypeTag> flux(*this, gridVariables, sol);

        const int numChannelsX = getParam<int>("Grid.NumChannelsX");
        const int numChannelsY = getParam<int>("Grid.NumChannelsY");

        const Scalar channelWidthX = getParam<Scalar>("Grid.ChannelWidthX");
        const Scalar pillarWidthX = getParam<Scalar>("Grid.PillarWidthX");
        const Scalar channelWidthY = getParam<Scalar>("Grid.ChannelWidthY");
        const Scalar pillarWidthY =  getParam<Scalar>("Grid.PillarWidthY");

        Scalar offsetX = this->fvGridGeometry().bBoxMin()[0] + 0.5*pillarWidthX;
        Scalar offsetY= this->fvGridGeometry().bBoxMin()[1] + 0.5*pillarWidthY;

        const auto element0 = *(elements(this->fvGridGeometry().gridView()).begin());
        const auto diagonal = (element0.geometry().corner(3) - element0.geometry().corner(0));
        const std::vector<Scalar> gridCellSize = {diagonal[0], diagonal[1]};

        assert(Dune::FloatCmp::eq(gridCellSize[0], gridCellSize[1], 1e-6));

        const std::vector<int> numCellsPerThroatLength = {
                                                            static_cast<int>(std::round(pillarWidthX / gridCellSize[0])),
                                                            static_cast<int>(std::round(pillarWidthY / gridCellSize[1]))
                                                         };

        const std::vector<int> numCellsPerThroatWidth = {
                                                            static_cast<int>(std::round(channelWidthY / gridCellSize[1])),
                                                            static_cast<int>(std::round(channelWidthX / gridCellSize[0]))
                                                        };

        try{ offsetX += getParam<Scalar>("FluxOverPlane.OffsetX"); }
        catch(Dumux::ParameterException& e)
        {
            if(numCellsPerThroatLength[0] % 2 != 0)
                offsetX += 0.5*gridCellSize[0];
        };

        try{ offsetY += getParam<Scalar>("FluxOverPlane.OffsetY"); }
        catch(Dumux::ParameterException& e)
        {
            if(numCellsPerThroatLength[1] % 2 != 0)
                offsetY += 0.5*gridCellSize[1];
        };

        for(int i = 0; i < numChannelsX + 1; ++i)
        {
            Scalar internalOffsetY = this->fvGridGeometry().bBoxMin()[1] + pillarWidthY;
            for(int j = 0; j < numChannelsY; ++j)
            {
                flux.addSurface("horizontalFlux_"+std::to_string(i), {offsetX, internalOffsetY}, {offsetX, internalOffsetY + channelWidthY});
                internalOffsetY += channelWidthY + pillarWidthY;
            }
            offsetX += channelWidthX + pillarWidthX;
        }

        for(int i = 0; i < numChannelsY + 1; ++i)
        {
            Scalar internalOffsetX = this->fvGridGeometry().bBoxMin()[0] + pillarWidthX;
            for(int j = 0; j < numChannelsX; ++j)
            {
                flux.addSurface("verticalFlux_"+std::to_string(i), {internalOffsetX, offsetY}, {internalOffsetX + channelWidthX, offsetY});
                internalOffsetX += channelWidthX + pillarWidthX;
            }
            offsetY += channelWidthY + pillarWidthY;
        }

        flux.calculateMassOrMoleFluxes();

        std::cout << "\n##########################\n";
        std::cout << "Horizontal Fluxes\n\n";
        for(int i = 0; i < numChannelsX + 1; ++i)
        {
            const std::string planeName = "horizontalFlux_"+std::to_string(i);
            const Scalar horizontalFlux = flux.netFlux(planeName);
            std::cout << "\nat plane " << i << ": " << horizontalFlux << std::endl;

            const auto& values = flux.values(planeName);
            std::cout << "Individual throat fluxes: \n";
            for(int i = 0; i < values.size(); ++i)
                std::cout << i << ": " << values[i] << std::endl;
        }

        std::cout << "\n##########################\n";
        std::cout << "Vertical Fluxes\n\n";
        for(int i = 0; i < numChannelsY + 1; ++i)
        {
            const std::string planeName = "verticalFlux_"+std::to_string(i);
            const Scalar verticalFlux = flux.netFlux(planeName);
            std::cout << "\nat plane " << i << ": " << verticalFlux << std::endl;

            const auto& values = flux.values(planeName);
            std::cout << "Individual throat fluxes: \n";
            for(int i = 0; i < values.size(); ++i)
                std::cout << i << ": " << values[i] << std::endl;
        }

        std::cout << std::endl;
        std::cout << "NumCells per throat (length / width): " << numCellsPerThroatLength[0] << " " << numCellsPerThroatWidth[0] << std::endl;
        std::cout << "numDofs: " << this->fvGridGeometry().numCellCenterDofs() + this->fvGridGeometry().numFaceDofs() << std::endl;

    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        auto isInlet = [&](const auto& pos)
        {
            if(flowDirection_ == "leftToRight")
             return isLeft(pos);
            else
                return isTop(pos);

        };

        if (isOutlet(globalPos) || isInlet(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 0;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;


        if(flowDirection_ == "leftToRight")
        {
            if(isLeft(globalPos))
                values[Indices::pressureIdx] = pLeft_;
        }

        else if(flowDirection_ == "diagonal")
        {
            if(isTop(globalPos))
                values[Indices::pressureIdx] = pLeft_ * (1.0 - (globalPos[0] / this->fvGridGeometry().bBoxMax()[0]));
        }

        else
            DUNE_THROW(ParameterException, "FlowDirection must be either 'leftToRight' or 'diagonal'");

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 0;//1.1e+5;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        return values;
    }

    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    {
        return extrusionFactor_;
    }

    // \}

private:

    bool isLeft(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < eps_;
    }

    bool isTop(const GlobalPosition& globalPos) const
    {
        return (globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_) ;
    }

    bool isOutlet(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_;
    }

    bool isWall(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > eps_ || globalPos[0] < this->fvGridGeometry().bBoxMax()[0] - eps_;
    }

    Scalar eps_;
    Scalar extrusionFactor_;
    Scalar inletLength_;
    Scalar heightCavity_;
    bool calculateFluxesOverPlane_;
    std::string flowDirection_;
    Scalar pLeft_;
};
} //end namespace

#endif
