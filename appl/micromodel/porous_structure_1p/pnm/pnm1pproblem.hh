// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P_PROBLEM_HH
#define DUMUX_PNM1P_PROBLEM_HH

// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/1p/model.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

namespace Dumux {

template<class FVGridGeometry, class Scalar, class Labels, Shape poreGeometry, Shape throatGeometry>
class StructuredSpatialParams;


template <class TypeTag>
class PNMOnePProblem;

namespace Properties {

NEW_TYPE_TAG(PNMOnePProblem, INHERITS_FROM(PNMOneP));

// Set the problem property
SET_TYPE_PROP(PNMOnePProblem, Problem, Dumux::PNMOnePProblem<TypeTag>);

// the fluid system
SET_PROP(PNMOnePProblem, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};


// Set the grid type
SET_TYPE_PROP(PNMOnePProblem, Grid, Dune::FoamGrid<1, 2>);

SET_PROP(PNMOnePProblem, PoreGeometry)
{
    static const Shape value = Shape::TwoPlates;
};

SET_PROP(PNMOnePProblem, ThroatGeometry)
{
    static const Shape value = Shape::TwoPlates;
};

SET_PROP(PNMOnePProblem, SpatialParams)
{
    using type = StructuredSpatialParams<typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                                  typename GET_PROP_TYPE(TypeTag, Scalar),
                                                  typename GET_PROP_TYPE(TypeTag, Labels),
                                                  GET_PROP_VALUE(TypeTag, PoreGeometry),
                                                  GET_PROP_VALUE(TypeTag, ThroatGeometry)>;
};

SET_TYPE_PROP(PNMOnePProblem, SinglePhaseTransmissibility, UserDefinedTransmissibilty<typename GET_PROP_TYPE(TypeTag, Scalar)>);

SET_BOOL_PROP(PNMOnePProblem, NeglectPoreFlowResistance, true);
}

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class FVGridGeometry, class Scalar, class Labels, Shape poreGeometry, Shape throatGeometry>
class StructuredSpatialParams : public PNMOnePSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry>
{
    using ParentType = PNMOnePSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry>;

public:

    using ParentType::ParentType;

    template<class Problem, class Element, class ElementVolumeVariables>
    Scalar throatTransmissibility(const Problem& problem, const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        static const Scalar upScaledThroatHydraulicResistance = 1.0 / getParamFromGroup<Scalar>(problem.paramGroup(), "Problem.TransmissibilityThroat");
        static const Scalar upScaledHalfPoreHydraulicResistance = 1.0 / getParamFromGroup<Scalar>(problem.paramGroup(), "Problem.TransmissibilityHalfPore");

        Scalar rHyd = upScaledThroatHydraulicResistance / 1e-3;

        for (int scvIdx = 0; scvIdx < 2; ++scvIdx)
        {
            const int dofIdxGlobal = this->fvGridGeometry().vertexMapper().subIndex(element, scvIdx, FVGridGeometry::GridView::dimension);
            const Scalar poreLabel = this->poreLabel(dofIdxGlobal);

            if (poreLabel == -1)
                rHyd += upScaledHalfPoreHydraulicResistance / 1e-3;
        }

        return 1.0 / rHyd; // viscosity will be accounted for later
    }
};

template <class TypeTag>
class PNMOnePProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);


    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<GridView::dimension>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    template<class SpatialParams>
    PNMOnePProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<SpatialParams> spatialParams)
    : ParentType(fvGridGeometry, spatialParams), eps_(1e-7)
    {
        flowDirection_ = getParam<std::string>("Problem.FlowDirection");
        pLeft_ = getParam<Scalar>("Problem.PLeft");
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const //define output
    {
        return true;
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C

    // \}
     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        const auto pos = element.geometry().corner(scv.indexInElement());

        if(flowDirection_ == "leftToRight")
        {
            if(pos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_ || pos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_)
                bcTypes.setAllDirichlet();

            else // neuman for the remaining boundaries
                bcTypes.setAllNeumann();
        }
        else
        {
            if(isTop_(pos) || pos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_)
                bcTypes.setAllDirichlet();

            else // neuman for the remaining boundaries
                bcTypes.setAllNeumann();
        }

        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = 0.0;
        const auto pos = element.geometry().corner(scv.indexInElement());


        if(flowDirection_ == "leftToRight")
        {
            if(pos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_)
                values[Indices::pressureIdx] = pLeft_;
        }

        else if(flowDirection_ == "diagonal")
        {
            if(isTop_(pos))
                values[Indices::pressureIdx] = pLeft_ * (1.0 - (pos[0] / this->fvGridGeometry().bBoxMax()[0]));
        }

        else
            DUNE_THROW(ParameterException, "FlowDirection must be either 'leftToRight' or 'diagonal'");

         return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    template<class ElementVolumeVariables>
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        return PrimaryVariables(0);
    }
    // \}

    // \}
private:

    bool isTop_(const GlobalPosition& globalPos) const
    {
        return (globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_) ;
    }

    Scalar eps_;
    std::string flowDirection_;
    Scalar pLeft_;
};
} //end namespace

#endif
