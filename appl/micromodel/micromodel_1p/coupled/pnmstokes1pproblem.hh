// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the 1d3d coupled problem:
 * a one-dimensional network is embedded in a three-dimensional matrix
 * Comparison with anaytical solutiom (see D'Angelo 2007 PhD thesis)
 */
#ifndef DUMUX_PNM_STOKES_1P_TEST_PROBLEM_HH
#define DUMUX_PNM_STOKES_1P_TEST_PROBLEM_HH

#include "pnmproblem.hh"
#include "stokestestproblem.hh"

#include <dumux/mixeddimension/problem.hh>
#include <dumux/mixeddimension/boundary/pnmstokes/couplingmanager.hh>
#include <dumux/mixeddimension/boundary/pnmstokes/couplingdata.hh>
#include <dumux/mixeddimension/boundary/pnmstokes/map.hh>
#include <dumux/mixeddimension/staggered/properties.hh>

#include<dumux/porenetworkflow/common/functions.hh>

#include <dumux/mixeddimension/staggered/model.hh>
#include <dumux/mixeddimension/staggered/assembler.hh>
#include <dumux/mixeddimension/staggered/newtoncontroller.hh>
#include <dumux/mixeddimension/staggered/subproblemlocaljacobian.hh>


namespace Dumux
{
template <class TypeTag>
class TestOneDThreeDProblem;

namespace Properties
{
NEW_TYPE_TAG(TestOneDThreeDProblem, INHERITS_FROM(MixedDimension, BoundaryCouplingBulkStaggeredModel));

// Set the problem property
SET_TYPE_PROP(TestOneDThreeDProblem, Problem, Dumux::TestOneDThreeDProblem<TypeTag>);

// Set the coupling manager
SET_TYPE_PROP(TestOneDThreeDProblem, CouplingManager, Dumux::BoundaryCouplingManagerPNMStokes<TypeTag>);

//////////////////////////////////////////////////////////////////////////
// Set the two sub-problems of the global problem
SET_TYPE_PROP(TestOneDThreeDProblem, LowDimProblemTypeTag, TTAG(PNMProblem));
SET_TYPE_PROP(TestOneDThreeDProblem, BulkProblemTypeTag, TTAG(StokesTestProblem));
////////////////////////////////////////////////////////////////////////////

// publish this problem in the sub problems
SET_TYPE_PROP(PNMProblem, GlobalProblemTypeTag, TTAG(TestOneDThreeDProblem));
SET_TYPE_PROP(StokesTestProblem, GlobalProblemTypeTag, TTAG(TestOneDThreeDProblem));

// The subproblems inherit the parameter tree from this problem
SET_PROP(PNMProblem, ParameterTree) : GET_PROP(TTAG(TestOneDThreeDProblem), ParameterTree) {};
SET_PROP(StokesTestProblem, ParameterTree) : GET_PROP(TTAG(TestOneDThreeDProblem), ParameterTree) {};

// SET_BOOL_PROP(TestOneDThreeDProblem, MultiDimensionUseIterativeSolver, true);

NEW_PROP_TAG(LowDimToBulkMapValue); // TODO: make specialized map value class
SET_TYPE_PROP(TestOneDThreeDProblem, LowDimToBulkMapValue, Dumux::PNMToStokesMap<TypeTag>);

NEW_PROP_TAG(BulkData);
SET_TYPE_PROP(TestOneDThreeDProblem, BulkData, StokesData<TypeTag>);

NEW_PROP_TAG(LowDimData);
SET_TYPE_PROP(TestOneDThreeDProblem, LowDimData, PNMData<TypeTag>);

//! Set the BaseModel to MixedDimensionModel
// SET_TYPE_PROP(TestOneDThreeDProblem, Model, MixedDimensionModelForStaggered<TypeTag>);
// SET_TYPE_PROP(TestOneDThreeDProblem, JacobianAssembler, MixedDimensionAssemblerForStaggered<TypeTag>);
// SET_TYPE_PROP(TestOneDThreeDProblem, NewtonController, MixedDimensionNewtonControllerForStaggered<TypeTag>);

#if HAVE_UMFPACK
SET_TYPE_PROP(TestOneDThreeDProblem, LinearSolver, UMFPackBackend<TypeTag>);
#endif

}//end namespace properties

template <class TypeTag>
class TestOneDThreeDProblem : public MixedDimensionProblem<TypeTag>
{
    using ParentType = MixedDimensionProblem<TypeTag>;
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    // obtain the type tags of the sub problems
    using BulkProblemTypeTag = typename GET_PROP_TYPE(TypeTag, BulkProblemTypeTag);
    using LowDimProblemTypeTag = typename GET_PROP_TYPE(TypeTag, LowDimProblemTypeTag);

    // obtain types from the sub problem type tags
    using BulkProblem = typename GET_PROP_TYPE(BulkProblemTypeTag, Problem);
    using LowDimProblem = typename GET_PROP_TYPE(LowDimProblemTypeTag, Problem);

    using BulkGridView = typename GET_PROP_TYPE(BulkProblemTypeTag, GridView);
    using LowDimGridView = typename GET_PROP_TYPE(LowDimProblemTypeTag, GridView);

    enum { bulkIsBox = GET_PROP_VALUE(BulkProblemTypeTag, ImplicitIsBox) };
    enum { lowDimIsBox = GET_PROP_VALUE(LowDimProblemTypeTag, ImplicitIsBox) };

    using BulkPrimaryVariables = typename GET_PROP_TYPE(BulkProblemTypeTag, PrimaryVariables);
    using LowDimPrimaryVariables = typename GET_PROP_TYPE(LowDimProblemTypeTag, PrimaryVariables);

public:
    TestOneDThreeDProblem(TimeManager &timeManager, const BulkGridView &bulkGridView, const LowDimGridView &lowDimgridView)
    : ParentType(timeManager, bulkGridView, lowDimgridView)
    {
        verbose_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Problem, Verbose);
    }


    bool shouldWriteOutput() const //define output
    {
        return true;
//         return (this->timeManager().willBeFinished());
    }

    void postTimeStep()
    {
        this->couplingManager().bulkProblem().postTimeStep();
        // calculate the mass flux entering the bulk domain
//         BulkPrimaryVariables inflow;
//         const auto left = this->couplingManager().bulkProblem().bBoxMin()[0] + 1e-3;
//         this->couplingManager().bulkProblem().calculateFluxAcrossLayer(inflow, 0, left);
//
        //calculate the mass flux leaving  the lowDim domain
        const LowDimPrimaryVariables outflow = Functions<LowDimProblemTypeTag>::boundaryFlux(this->couplingManager().lowDimProblem(), "max", 1);
//
//         if(verbose_)
//         {
//             std::cout << "IN Darcy: " << inflow << " kg/s" << std::endl;
            std::cout << "OUT PNM : " << outflow <<  " kg/s"<<  std::endl;
//         }
//         // at the end of the simulation, check if everything is mass conservative
//         if(this->timeManager().willBeFinished())
//         {
//             if(Dune::FloatCmp::ne<Scalar>(inflow, outflow, 1e-6))
//             {
//                 std::cout << "Test failed: Inflow differs from outflow: " << inflow - outflow << std::endl;
//                 std::exit(EXIT_FAILURE);
//             }
//         }
    }
private:
    bool verbose_;
};

} //end namespace

#endif
