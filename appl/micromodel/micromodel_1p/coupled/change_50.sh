#!/bin/bash

#make backup
cp 50_micron_coupled_micromodel_2d.input 50_micron_coupled_micromodel_2d.bak

# change 50
sed -i "s/CellsOutletX = 250/CellsOutletX = 245/g" 50_micron_coupled_micromodel_2d.input
sed -i "s/CellsOutletX = 500/CellsOutletX = 490/g" 50_micron_coupled_micromodel_2d.input
sed -i "s/CellsOutletX = 1000/CellsOutletX = 980/g" 50_micron_coupled_micromodel_2d.input
sed -i "s/LowerLeft = -2.525e-3 5.05e-3 # inlet length: 0.25e-2 m/LowerLeft = 0 5e-3 # inlet length: 0.25e-2 m/g" 50_micron_coupled_micromodel_2d.input
sed -i "s/UpperRight = 0.01255 6.05e-3 # outlet length: 0.25e-2 m, heigth channel: 1e-3 m/UpperRight = 0.015 6e-3 # outlet length: 0.25e-2 m, heigth channel: 1e-3 m/g" 50_micron_coupled_micromodel_2d.input
sed -i "s/LowerLeft = 0 0/LowerLeft = 0.002525 0/g" 50_micron_coupled_micromodel_2d.input
sed -i "s/UpperRight = 10.05e-3 5.05e-3 # 20 x 10 blocks, block size 400e-6 m/UpperRight = 0.012525 0.005 # 20 x 10 blocks, block size 400e-6 m/g" 50_micron_coupled_micromodel_2d.input
