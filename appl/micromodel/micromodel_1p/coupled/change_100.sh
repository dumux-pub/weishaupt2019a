#!/bin/bash

#make backup
cp 100_micron_coupled_micromodel_2d.input 100_micron_coupled_micromodel_2d.bak

# change 100
sed -i "s/CellsOutletX = 125/CellsOutletX = 120/g" 100_micron_coupled_micromodel_2d.input
sed -i "s/CellsOutletX = 250/CellsOutletX = 240/g" 100_micron_coupled_micromodel_2d.input
sed -i "s/CellsOutletX = 500/CellsOutletX = 480/g" 100_micron_coupled_micromodel_2d.input
sed -i "s/LowerLeft = -2.55e-3 5.4e-3 # inlet length: 0.25e-2 m/LowerLeft = -2.3e-3 5e-3 # inlet length: 0.25e-2 m/g" 100_micron_coupled_micromodel_2d.input
sed -i "s/UpperRight = 0.0129 6.4e-3 # outlet length: 0.25e-2 m, heigth channel: 1e-3 m/UpperRight = 0.0127 6e-3 # outlet length: 0.25e-2 m, heigth channel: 1e-3 m/g" 100_micron_coupled_micromodel_2d.input
sed -i "s/LowerLeft = 0 0/LowerLeft = 250e-6 0/g" 100_micron_coupled_micromodel_2d.input
sed -i "s/UpperRight = 10.4e-3 5.4e-3 # 20 x 10 blocks, block size 400e-6 m/UpperRight = 0.01025 0.005 # 20 x 10 blocks, block size 400e-6 m/g" 100_micron_coupled_micromodel_2d.input
