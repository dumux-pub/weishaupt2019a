def readFile(fileName):
    with open(fileName) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    return content

def getPlaneFluxes(fileName) :

    throatFluxes = []


    content = readFile(fileName)
    # get index for end of information

    # for index, value in enumerate(content):
    #     if value.startswith("NumCells per throat"):
    #         indexEnd = index
    #         break
    for line in content:
        if line.startswith('throat'):
            # print line.split(':')[-1]
            throatFluxes.append(float(line.split(':')[-1].strip()))

    # print throatFluxes
    return throatFluxes


# getPlaneFluxes('100_05.log')
