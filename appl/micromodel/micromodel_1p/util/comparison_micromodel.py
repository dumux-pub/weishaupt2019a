import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import sys
sys.path.append("./../porous_structure_1p/")
from functions import *

import micromodel_functions as mf

import glob

import numpy as np


def graph(dataCouples):
    # matplotlib.rc('text', usetex = True)

    # plt.style.use('ggplot')
    # plt.style.use('grayscale')

    fig = plt.figure(figsize=(5,5))
    sub1 = fig.add_subplot(1,1,1)
    bar_width = 1.0

    for figIdx, couple in enumerate(dataCouples):
        filenamesReference = sorted(glob.glob(couple[0] + '*log'))
        filesnamesCoupled =sorted(glob.glob(couple[1] + '*log'))

        throatSize = filenamesReference[0].split('/')[-1].split('_')[1]

        # # extract the fluxes and the file names
        fluxesReference = [ mf.getPlaneFluxes(x) for x in filenamesReference]
        fluxesCoupled = [ mf.getPlaneFluxes(x) for x in filesnamesCoupled]

        cumulativeFluxRef = 0.0
        cumulativeFluxCoupled = 0.0

        for flux in fluxesReference[-1]:
            cumulativeFluxRef += 0.5*abs(flux)

        for flux in fluxesCoupled[-1]:
            cumulativeFluxCoupled += 0.5*abs(flux)


        def relativeError(val, ref):
            return (val-ref)/ref*100.00


        #the vertical fluxes
        labelRef = "reference" if figIdx == 0 else ""
        labelCoupled = "coupled" if figIdx == 0 else ""

        sub1.bar(figIdx*3*bar_width - 0.5*bar_width, cumulativeFluxRef, bar_width, log=True, color=tableau20[0], alpha=1, label=labelRef)
        sub1.bar(figIdx*3*bar_width + 0.5*bar_width, cumulativeFluxCoupled, bar_width, log=True, color=tableau20[2], alpha=1, label=labelCoupled)

        sub1.set_yscale('log')
        sub1.set_ylim([1*10e-13,5*10e-10])

        sub1.set_xticks([0,3,6])
        sub1.set_xlabel('throat diameter [$\mathsf{\mu}$m]')
        sub1.set_xticklabels(['50', '100', '250'])
        sub1.legend(ncol=2)

        sub1.set_ylabel('cumulative vertical flux ['r'$\mathsf{m^3/(m s)}}$]')
        relErr = relativeError(cumulativeFluxCoupled, cumulativeFluxRef)
        sub1.text(figIdx*3*bar_width - 0.5*bar_width, 1.2*cumulativeFluxCoupled, '{:+.2f}'.format(relErr)+" %", color='black')



        namesReference = [ name.split('/')[-1].split('.')[0] for name in filenamesReference]
        namesCoupled = [ name.split('/')[-1].split('.')[0] for name in filesnamesCoupled]

        def numThroatsReference(idx):
            return int(namesReference[idx].split('_')[-1])

        def numThroatsCoupled(idx):
            return int(namesCoupled[idx].split('_')[-1])

    fig.tight_layout()
    fig.savefig('comparison.pdf')



dataCouples = [['./ref_50/', './coupled_50/'], ['./ref_100/', './coupled_100/'],['./ref_250/', './coupled_250/']]

graph(dataCouples)
