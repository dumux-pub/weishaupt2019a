import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import sys
sys.path.append("./../porous_structure_1p/")
# from functions import *

from micromodel_functions import *

import glob

import numpy as np


def graph(directoryRef, directoryCoupled):
    # matplotlib.rc('text', usetex = True)

    # plt.style.use('ggplot')
    # plt.style.use('grayscale')
    filenamesReference = sorted(glob.glob(directoryRef + '*log'))
    filesnamesCoupled =sorted(glob.glob(directoryCoupled + '*log'))

    throatSize = filenamesReference[0].split('/')[-1].split('_')[1]

    # # extract the fluxes and the file names
    fluxesReference = [ getPlaneFluxes(x) for x in filenamesReference]
    fluxesCoupled = [ getPlaneFluxes(x) for x in filesnamesCoupled]

    # print fluxesCoupled
    # test = np.array(fluxesCoupled[0], dtype="float32")
    # print test[0]

    # for coupled, ref in zip(fluxesCoupled[0], fluxesReference[0]):
    #     print coupled, ref

    def relativeError(val, ref):
        return (val-ref)/ref*100.00

    relativeDiff = []
    relativeDiffToFineRef = []

    for valuesCoupled, valuesRef in zip(fluxesCoupled, fluxesReference):
        relativeDiff.append([relativeError(valCoupled, valRef) for valCoupled, valRef in zip(valuesCoupled, valuesRef)])
        relativeDiffToFineRef.append([relativeError(valCoupled, valRef) for valCoupled, valRef in zip(valuesCoupled, fluxesReference[-1])])



    namesReference = [ name.split('/')[-1].split('.')[0] for name in filenamesReference]
    namesCoupled = [ name.split('/')[-1].split('.')[0] for name in filesnamesCoupled]

    def numThroatsReference(idx):
        return int(namesReference[idx].split('_')[-1])

    def numThroatsCoupled(idx):
        return int(namesCoupled[idx].split('_')[-1])

    # create a figure (2x2)
    fig1 = plt.figure(figsize=(10,10))
    sub1 = fig1.add_subplot(2,2,1)
    sub2 = fig1.add_subplot(2,2,2)

    # # plot the two upper figures
    xUpperLeftReference = range(len(fluxesReference[0]))
    xUpperLeftCoupled = range(len(fluxesCoupled[0]))

    # xUpperRight = range(len(fluxes[0]['planeSubFluxesX'][4]))
    # labels = [int(i.split('_')[-1]) for i in data]
    for idx, val in enumerate(fluxesReference):
        sub1.plot(xUpperLeftReference, val, 'o', label=numThroatsReference(idx))
        #     sub2.plot(xUpperRight, val['planeSubFluxesY'][4], 'o', label=numThroats(idx))
    for idx, val in enumerate(fluxesCoupled):
        sub1.plot(xUpperLeftCoupled, val, 'x', label=numThroatsCoupled(idx))

    for idx, val in enumerate(relativeDiff):
    # for idx, val in enumerate(relativeDiffToFineRef):
        sub2.plot(xUpperLeftCoupled, val, 'x', label=numThroatsCoupled(idx))

    sub2.set_ylim(bottom = 0)

    sub2.set_title("Relative difference to equivalent ref. solution")
    sub2.set_ylabel('Relative error 'r'$q_{coupled} - q_{ref}$ [%]')
    sub2.legend(prop={'size':8}, ncol= 2,numpoints=1, title='cells per throat width')

    # # upper left figure
    sub1.set_title('Verticel fluxes over interface')
    sub1.set_ylabel('Specific flux per throat ['r'$\mathsf{m^3/(m s)}}$]')
    sub1.legend(loc='upper center',prop={'size':8}, ncol= 2,numpoints=1, title='cells per throat width\nleft: ref.  right: coupled')
    # sub1.set_xlabel('throat')
    sub1.set_xlabel('throat')
    sub2.set_xlabel('throat')
    # sub3.set_xlabel('throat')
    # sub4.set_xlabel('throat')
    # sub1.set_xticklabels(tick_labels.astype(int))
    sub1.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    sub2.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))


    fig1.tight_layout()
    fig1.subplots_adjust(top=0.88)
    fig1.savefig('./convergence_' + throatSize + '.pdf', bbox_inches='tight')



graph('./ref_250/', './coupled_250/')
graph('./ref_100/', './coupled_100/')
