// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Auxiliary class used to select the elements for the final grid
 */
#ifndef DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH
#define DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH

#include <string>
#include <dune/common/fvector.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dumux/common/geometry/intersectspointgeometry.hh>

namespace Dumux
{

/*!
 * \brief Auxiliary class used to select the elements for the final grid
 */
template <class GridView>
class ElementSelector
{
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;

    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using Rectangle = Dune::AxisAlignedCubeGeometry<Scalar, dim, dim>;

public:
    ElementSelector(const std::string& modelParamGroup = "")
    {
        const Scalar lengthCavity = getParamFromGroup<Scalar>(modelParamGroup, "Grid.CavityLength");
        const Scalar heightCavity = getParamFromGroup<Scalar>(modelParamGroup, "Grid.CavityHeight");

        const GlobalPosition upperRight = getParamFromGroup<GlobalPosition>(modelParamGroup, "Grid.UpperRight");

        const int numPillarsX = getParamFromGroup<int>(modelParamGroup, "Grid.NumPillarsX");
        const int numPillarsY = getParamFromGroup<int>(modelParamGroup, "Grid.NumPillarsY");

        const int numChannelsX = numPillarsX + 1;
        const int numChannelsY = numPillarsY;

        const Scalar channelWidthX = getParamFromGroup<Scalar>(modelParamGroup, "Grid.ChannelWidthX");
        const Scalar channelWidthY = getParamFromGroup<Scalar>(modelParamGroup, "Grid.ChannelWidthY");

        const Scalar pillarWidthX = (lengthCavity - numChannelsX*channelWidthX)/numPillarsX;
        const Scalar pillarWidthY = (heightCavity - numChannelsY*channelWidthY)/numPillarsY;

        // std::cout << std::setprecision(15) << "lengthCavity " << lengthCavity << std::endl;
        // std::cout << std::setprecision(15) << "numChannelsX " << numChannelsX << std::endl;
        // std::cout << std::setprecision(15) << "channelWidthX " << channelWidthX << std::endl;
        // std::cout << std::setprecision(15) << "numPillarsX " << numPillarsX << std::endl;
        // std::cout << std::setprecision(15) << "pillarWidthX " << pillarWidthX << std::endl;

        const Scalar inletLength = getParamFromGroup<Scalar>(modelParamGroup, "Grid.InletLength");

        Scalar offsetX = inletLength;
        Scalar offSetY = 0.0;


        Rectangle belowInlet(GlobalPosition{0.000, 0.000}, GlobalPosition{inletLength, heightCavity});
        Rectangle belowOutlet(GlobalPosition{inletLength+lengthCavity, 0.000}, GlobalPosition{upperRight[0], heightCavity});

        list_.push_back(belowInlet);
        list_.push_back(belowOutlet);

        for(int iX = 0; iX < numPillarsX; ++iX)
        {
            const Scalar lowerLeftX = offsetX+channelWidthX;
            const Scalar upperRightX = lowerLeftX+pillarWidthX;
            for(int iY = 0; iY < numPillarsY; ++iY)
            {
                const Scalar lowerLeftY = offSetY+channelWidthY;
                const Scalar upperRightY = lowerLeftY+pillarWidthY;
                list_.emplace_back(GlobalPosition{lowerLeftX, lowerLeftY}, GlobalPosition{upperRightX, upperRightY});
                offSetY += channelWidthY+pillarWidthY;
            }
            offsetX += channelWidthX+pillarWidthX;
            offSetY = 0.0;
        }
    }


    //! Select all elements within a circle around a center point.
    bool operator() (const auto& element) const
    {
        const auto center = element.geometry().center();

        for(auto&& rectangle : list_)
        {
            if(intersectsPointGeometry(center, rectangle))
                return false;
        }

        return true;
    }

private:
    std::vector<Rectangle> list_;
};

} //end namespace

#endif
