// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_CHANNEL_PROBLEM_HH
#define DUMUX_CHANNEL_PROBLEM_HH

#include <string>

#include <dune/grid/yaspgrid.hh>
#include <dumux/io/grid/subgridgridcreator.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>

namespace Dumux
{
template <class TypeTag>
class StructuredProblem;

namespace Properties
{
NEW_TYPE_TAG(StructuredProblem, INHERITS_FROM(StaggeredFreeFlowModel, NavierStokes));

// the fluid system
SET_PROP(StructuredProblem, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
SET_PROP(StructuredProblem, Grid)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    static constexpr auto dim = 2;

public:
    // using HostGrid = Dune::YaspGrid<dim>;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the problem property
SET_TYPE_PROP(StructuredProblem, Problem, Dumux::StructuredProblem<TypeTag> );

SET_BOOL_PROP(StructuredProblem, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(StructuredProblem, EnableGridFluxVariablesCache, true);
SET_BOOL_PROP(StructuredProblem, EnableGridVolumeVariablesCache, true);

}

/*!
 * \brief  Test problem for the one-phase model:
   \todo doc me!
 */
template <class TypeTag>
class StructuredProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);

    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using Element = typename GridView::template Codim<0>::Entity;

    using CellCenterPrimaryVariables = typename GET_PROP_TYPE(TypeTag, CellCenterPrimaryVariables);
    using FacePrimaryVariables = typename GET_PROP_TYPE(TypeTag, FacePrimaryVariables);

    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);

public:
    StructuredProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), eps_(1e-6)
    {

        extrusionFactor_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.ExtrusionFactor");

        inletLength_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.InletLength");
        heightCavity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.CavityHeight");
        pLeft_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PressureLeft");
        calculateFluxesOverPlane_ = getParamFromGroup<bool>(this->paramGroup(), "FluxOverPlane.CalculateFluxes");
    }

    /*!
     * \name Problem parameters
     */
    // \{


    template<class GridVariables, class SolutionVector>
    void getFluxes(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        // if(this->grid().maxLevel() > 0)
        //     FineToCoarseGridWriter::writeFineToCoarseGrid(*this);

        if(!calculateFluxesOverPlane_)
            return;

        // get some data concerning the geometry
        const Scalar lengthCavity = getParam<Scalar>("Grid.CavityLength");
        const int numPillarsX = getParam<int>("Grid.NumPillarsX");
        const int numChannelsX = numPillarsX + 1;
        const Scalar channelWidthX = getParam<Scalar>("Grid.ChannelWidthX");
        const Scalar pillarWidthX = (lengthCavity - numChannelsX*channelWidthX)/numPillarsX;
        const Scalar inletLength = getParam<Scalar>("Grid.InletLength");

        // We store the horizontal volume flux in the inlet channel to check whether this fits with the applied BC and in order to
        // calculate a ratio (see below).
        FluxOverSurface<TypeTag> flux(*this, gridVariables, sol);

        const auto element0 = *(elements(this->fvGridGeometry().gridView()).begin());
        const auto diagonal = (element0.geometry().corner(3) - element0.geometry().corner(0));
        const std::vector<Scalar> gridCellSize = {diagonal[0], diagonal[1]};

        Scalar offsetInletPlane =  0.5*inletLength;
        const int numCellsInletX = static_cast<int>(std::round(inletLength / gridCellSize[0]));
        try{ offsetInletPlane += getParam<Scalar>("FluxOverPlane.VerticalPlaneX"); }
        catch(Dumux::ParameterException& e)
        {
            if(numCellsInletX % 2 != 0)
                offsetInletPlane += 0.5*gridCellSize[0];
        };

        flux.addSurface("inletHorizontalFlux", {offsetInletPlane, 0.0}, {offsetInletPlane, this->fvGridGeometry().bBoxMax()[1]});

        // We want to store the vertical fluxes in the first row of the vertical pore throats adjacent to the free flow channel.
        // Therefore, we need a sub-plane for each throat.
        // First, prepare a vector containing the geometries of the horizontal subplanes (leftX and rightX).
        // a horizontal plane (normal = {0 1})
        const Scalar horizontalPlaneY = getParam<Scalar>("FluxOverPlane.HorizontalPlaneY");
        const int numHorizontalSubPlanes = numChannelsX;
        Scalar offsetX = inletLength;

        for(int i = 0; i < numHorizontalSubPlanes; ++i)
        {
            const GlobalPosition p1 = {offsetX, horizontalPlaneY};
            const GlobalPosition p2 = {offsetX + channelWidthX, horizontalPlaneY};

            flux.addSurface("throatsVerticalFlux", p1, p2 );
            offsetX += channelWidthX + pillarWidthX;
        }

        flux.calculateMassOrMoleFluxes();

        const Scalar horizontalFlux = flux.netFlux("inletHorizontalFlux");

        std::cout << "\n##################################\n";
        std::cout << "\nFluxes:\n";
        std::cout << "\nhorizontal: " << horizontalFlux << "\n" <<  std::endl;

        const auto& values = flux.values("throatsVerticalFlux");

        for(int i = 0; i < values.size(); ++i)
            std::cout << "throat " << i << " : " << values[i] << std::endl;

        std::cout << "\nvertical net flux (should be zero): "  << flux.netFlux("throatsVerticalFlux") << std::endl;
        const Scalar verticalCumulativeFlux = std::accumulate(values.begin(), values.end(), 0.0, [](auto result, auto val) { return result + std::abs(val); });
        std::cout << "vertical cumulative flux " << verticalCumulativeFlux << std::endl;
        std::cout << "ratio vertical/horizontal: " << verticalCumulativeFlux/horizontalFlux << std::endl;
        std::cout << "\n##################################\n" << std::endl;
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set a fixed pressure at the inlet and outlet
        if (isInlet_(globalPos) || isOutlet_(globalPos))
            values.setDirichlet(Indices::pressureIdx);
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 0;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        if(isInlet_(globalPos))
            values[Indices::pressureIdx] = pLeft_;

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 0;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        return values;
    }

    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    {
        return extrusionFactor_;
    }

    // \}

private:

    bool isInlet_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_;
    }

    bool isTop_(const GlobalPosition& globalPos) const
    {
        return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_;
    }

    bool isOutlet_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_;
    }

    bool isWall(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > eps_ || globalPos[0] < this->fvGridGeometry().bBoxMax()[0] - eps_;
    }

    Scalar eps_;
    Scalar extrusionFactor_;
    Scalar inletLength_;
    Scalar heightCavity_;
    Scalar pLeft_;
    bool calculateFluxesOverPlane_;
};
} //end namespace

#endif
