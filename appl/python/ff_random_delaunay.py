from scipy.spatial import Delaunay
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import operator
from sets import Set

lowerLeft = [0.0, 0.0]
upperRight = [1.0, 1.0]

numPointsCenter = 60
numPointsTop = 10


def neighborIndices(k):
    indptr = tri.vertex_neighbor_vertices[1]
    indices = tri.vertex_neighbor_vertices[0]
    return indptr[indices[k]:indices[k+1]]


x=np.array(np.linspace(lowerLeft[0], upperRight[0], numPointsTop ))
y=np.array([1.0]*numPointsTop)



# np.random.seed(0)


x = np.append(x, np.random.uniform(low=lowerLeft[0], high=upperRight[0], size=numPointsCenter))
y = np.append(y, np.random.uniform(low=lowerLeft[1], high=upperRight[1], size=numPointsCenter))


tri = Delaunay(zip(x, y))

def coord(k):
    return tri.points[k]

def coordNum(k):
    return len(neighborIndices(k))

def onTop(pos):
    return pos[1] > upperRight[1] - 1e-6


throats = Set()
topPoreAdded = [False]*len(tri.points)

# Start with the vertices at the top. We want to make sure that each top pore is only connected to one throat
# (coordination number = 1)
for vIdx in range(numPointsTop):

    # Sort the neighboring pores by their distance to the pore at hand
    minDist = {}

    for neighborvIdx in neighborIndices(vIdx):
        minDist[neighborvIdx] = np.linalg.norm(coord(vIdx) - coord(neighborvIdx))

    minDist = sorted(minDist.items(), key=operator.itemgetter(1))

    # Get the global index of the nearest pore. If that pore lies on the top, try to get
    # the index of the second closest pore (and so on ...)
    nearestNeighborIdx = minDist[0][0]
    nearestNeighBorIsOnTop = True
    tmpIdx = 0

    while(nearestNeighBorIsOnTop):
        if(coord(nearestNeighborIdx)[1] > upperRight[1] - 1e-6):
            tmpIdx += 1
            nearestNeighborIdx = minDist[tmpIdx][0]
        else:
            nearestNeighBorIsOnTop = False

    # Now we have found the closest pore that does not lie on the top.
    # Get the respective pore indices and add the throat to the list.
    firstIdx = min(vIdx, nearestNeighborIdx)
    secondIdx = max(vIdx, nearestNeighborIdx)

    if(topPoreAdded[firstIdx] or topPoreAdded[secondIdx]):
        continue
    if(coord(firstIdx)[1] > upperRight[1] - 1e-6):
        topPoreAdded[firstIdx] = True
    if(coord(secondIdx)[1] > upperRight[1] - 1e-6):
        topPoreAdded[secondIdx] = True

    val = (firstIdx, secondIdx)
    throats.add(val)


# Handle the remaining pores
for vIdx, point in enumerate(tri.points):
    for neighbor in neighborIndices(vIdx):

        coordinationNumber = len(neighborIndices(vIdx))

        if coordinationNumber > 3:
            if np.random.rand(1) > 0.5:
                continue

        firstIdx = min(vIdx, neighbor)
        secondIdx = max(vIdx, neighbor)

        if(topPoreAdded[firstIdx] or topPoreAdded[secondIdx]):
               continue

        if(coord(firstIdx)[1] > upperRight[1] - 1e-6):
            topPoreAdded[firstIdx] = True
        if(coord(secondIdx)[1] > upperRight[1] - 1e-6):
            topPoreAdded[secondIdx] = True

        val = (firstIdx, secondIdx)
        throats.add(val)


# Plot the resulting network
for throat in throats:
    coord0 = coord(throat[0])
    coord1 = coord(throat[1])

    plt.plot([coord0[0],coord1[0]], [coord0[1],coord1[1]], 'k-')

# plt.plot(tri.points[:,0], tri.points[:,1], 'o')
# plt.triplot(tri.points[:,0], tri.points[:,1], tri.simplices.copy())
# plt.show()


# Write the actual DGF file
with open('mygrid.dgf', 'w') as gridfile:
    gridfile.write('DGF\nVertex % Coordinates and volumes of the pore bodies\n')
    gridfile.write('parameters 2\n')

    for point in tri.points:
        pointCoord = point
        if len(pointCoord) < 3:
            pointCoord = np.append(point, 0.0)

        poreRadius = 0.123 # TODO
        poreLabel = -1

        if(onTop(pointCoord)):
            poreLabel = 2  if pointCoord[0] < 0.5 else 3


        gridfile.write(str(pointCoord[0]) + ' ' + str(pointCoord[1]) + ' ' + str(pointCoord[2]) + ' ' + str(poreRadius) + ' ' + str(poreLabel) + '\n')
    gridfile.write('#\nSIMPLEX % Connections of the pore bodies (pore throats)\n')
    gridfile.write('parameters 2\n')

    for throat in throats:
        throatRadius = 1e-5 #TODO
        throatLength = 1e-5 #TODO
        gridfile.write(str(throat[0]) + ' ' + str(throat[1]) + ' ' + str(throatRadius) + ' ' + str(throatLength)  + '\n')

    gridfile.write('#')
