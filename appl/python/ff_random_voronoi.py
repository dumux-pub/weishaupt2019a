from scipy.spatial import Voronoi, voronoi_plot_2d, distance
import scipy.stats as stats
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import operator
from sets import Set
import bounded_voronoi
from truncated_lognormal import *

def averagedThroatRadius(poreRadiusOne, poreRadiusTwo, centerTocenterDist, n = 0.1):
        assert poreRadiusOne > 0.0
        assert poreRadiusTwo > 0.0
        assert centerTocenterDist > 0.0
        rOneTilde = poreRadiusOne/centerTocenterDist
        rTwoTilde = poreRadiusTwo/centerTocenterDist
        a = np.sin(np.pi/4.0)
        b = np.cos(np.pi/4.0)
        rhoOne = (rOneTilde*a) / ((1.0 - rOneTilde*b)**n)
        rhoTwo = (rTwoTilde*a) / (1.0 - rTwoTilde*b)**n
        rTilde = rhoOne*rhoTwo * pow((pow(rhoOne, 1.0/n) + pow(rhoTwo, 1.0/n)), -n)
        return rTilde * centerTocenterDist;

def makeGrid(domainSeed=0, parameterSeed=0):

    ## parameters #########################################################################
    dimWorld = 2

    # Set the dimensions of the network
    lowerLeft = [0.0, 0.0]
    upperRight = [1.0e-2, 0.5e-2]

    # Set the random states for the point in the domain and on the top
    randomDomainState = np.random.RandomState(domainSeed)
    randomTopState = np.random.RandomState(46)

    numPointsCenter = 210
    numPointsTop = 20
    # Get random positions for the pores on the top: Use uniform or truncated normal dist or set manually

    # uniform
    # xTop = sorted(randomTopState.uniform(low=lowerLeft[0], high=upperRight[0], size=numPointsTop))

    # truncated normal
    mu = 0.5e-2
    sigma = 1e-2
    # xTop = sorted(stats.truncnorm((lowerLeft[0] - mu) / sigma, (upperRight[0] - mu) / sigma, loc=mu, scale=sigma).rvs(numPointsTop, random_state = randomTopState))

    xTop = np.linspace(lowerLeft[0], upperRight[0], numPointsTop)

    addTopPoints = [(pos, upperRight[1]) for pos in xTop]

    # manual
    # print xTop
    # addTopPoints = [(0.01, 1), (0.09, 1), (0.5, 1)]

    avgPoreRadius = 1e-4
    stdDevPoreRadius = 1e-4
    minPoreRadius = 0.5e-4
    maxPoreRadius = 3e-4

    minThroatLength = 0.1 # fraction of center to center distance
    minCenterToCenterDistance = 1e-4

    variance = stdDevPoreRadius**2
    muPore = np.log(avgPoreRadius/np.sqrt(1+variance/(avgPoreRadius**2)))
    sigmaPore = np.sqrt(np.log(1+variance/(avgPoreRadius**2)))

    gridname = "voronoi_" + str(domainSeed) + "_"
    showGrid = True

    # specifify whether to ignore the pore on the top positioned by the Voronoi algorithm itself
    negletNonUserSpecifiedPointsOnTop = True

    ## parameters #########################################################################

    eps_ = 1e-15#1e-8


    def onTop(pos):
        return pos[1] > upperRight[1] - eps_

    x=np.array
    y=np.array

    x = np.append(x, randomDomainState.uniform(low=lowerLeft[0], high=upperRight[0], size=numPointsCenter))
    y = np.append(y, randomDomainState.uniform(low=lowerLeft[1], high=upperRight[1], size=numPointsCenter))

    bounding_box  = np.array([lowerLeft[0], upperRight[0], lowerLeft[1], upperRight[1]])
    vor = bounded_voronoi.voronoi(np.column_stack((x,y)), bounding_box)

    filteredThroats = []
    oldIdxIncluded = []

    coords = []
    pointIdx = {}
    posToOldIdxMap = {}

    # Extract pores and throats from the Voronoi diagram
    for throat in vor.ridge_vertices:
            x0 = vor.vertices[throat[0]][0]
            x1 = vor.vertices[throat[1]][0]
            y0 = vor.vertices[throat[0]][1]
            y1 = vor.vertices[throat[1]][1]

            if negletNonUserSpecifiedPointsOnTop:
                if y0 > upperRight[1] - eps_ or y1 > upperRight[1] - eps_:
                    continue

            if y0 > upperRight[1] - eps_ and y1 > upperRight[1] - eps_:
                continue

            if (x0 + x1)/2.0 < lowerLeft[0] + eps_ or (x0 + x1)/2.0 > upperRight[0] - eps_:
                continue

            if (y0 + y1)/2.0 < lowerLeft[1] + eps_ or (y0 + y1)/2.0 > upperRight[1] - eps_:
                continue

            # if min(x0, x1) < lowerLeft[0] + eps_ or max(x0, x1) > upperRight[0] - eps_:
            #     continue
            # if min(y0, y1) < lowerLeft[1] + eps_:# or max(y0, y1) > upperRight[1] - eps:
            #     continue

            filteredThroats.append(throat)
            posToOldIdxMap[(x0,y0)] = throat[0]
            posToOldIdxMap[(x1,y1)] = throat[1]

            if not throat[0] in oldIdxIncluded:
                oldIdxIncluded.append(throat[0])
                pointIdx[throat[0]] = len(oldIdxIncluded)-1
                coords.append((x0,y0))
            if not throat[1] in oldIdxIncluded:
                oldIdxIncluded.append(throat[1])
                pointIdx[throat[1]] = len(oldIdxIncluded)-1
                coords.append((x1,y1))

    # For a given point, return the closest point from a given set of points,
    # neglecting points on the top of the domain
    def closest_node(node, nodes):
        # Helper lambda to calculate the distance between to point. Returns a very large number (infinity) for points
        # on the top of the domain.
        dist = lambda pos0 , pos1: pos0 - pos1 if not onTop(pos1) else pos0 - (pos1[0], 1e9)
        # Use scipy's cdist with the lambda to get the closest point.
        return nodes[distance.cdist([node], nodes, lambda u, v: np.sqrt((dist(u,v)**2).sum())).argmin()]

    # Function to add the user-defined points and the respective throats on the top of the domain
    def addPoint(newPoint):
        clostedPointPos = closest_node(newPoint, oldCoords)
        clostPointOldIdx = posToOldIdxMap[clostedPointPos]
        coords.append(newPoint)
        newPointIdx = len(coords)-1
        filteredThroats.append([clostPointOldIdx, addPoint.offSetIdx])
        pointIdx[addPoint.offSetIdx] = newPointIdx
        addPoint.offSetIdx += 1
    addPoint.offSetIdx = max(pointIdx.keys())+1

    # Add the user-defined points
    oldCoords = coords
    for point in addTopPoints:
        addPoint(point)

    # Contract edges to avoid too short throats
    vertices = [[x, idx] for idx, x in enumerate(coords)]
    originalPoreIdxToNewPoreIdx = range(len(coords))
    originalThroatList = []
    useThroat = [True]*len(filteredThroats)
    newPoreIdx = len(coords)

    for throatIdx, throat in enumerate(filteredThroats):
        tmpIndices = [pointIdx[throat[0]], pointIdx[throat[1]]]
        idx0 = min(tmpIndices)
        idx1 = max(tmpIndices)
        originalThroatList.append([idx0, idx1])

        # get the throat length
        pos0 = coords[idx0]
        pos1 = coords[idx1]
        centerToCenterDistance = distance.euclidean(pos0, pos1)

        if centerToCenterDistance < minCenterToCenterDistance:
            # find the current index of the vertex which shall be deleted and remove it from the list
            tmpVertexIndices = [x[1] for x in vertices]

            # only delete throat and pores if none of the throat's adjacent pores have been deleted yet
            if idx0 in tmpVertexIndices and idx1 in tmpVertexIndices:
                useThroat[throatIdx] = False
                tmpIdx0 = tmpVertexIndices.index(idx0)
                tmpIdx1 = tmpVertexIndices.index(idx1)
                print "deleting throat ", throatIdx, " and pore ", idx1, " at pos ", vertices[tmpIdx1]
                # get middle between the two old points and assign to the remaining point
                midPoint = ((pos0[0] + pos1[0]) / 2.0, (pos0[1] + pos1[1]) / 2.0)
                vertices[tmpIdx0][0] = midPoint
                del vertices[tmpIdx1]
                del vertices[tmpIdx0]
                vertices.append([midPoint, newPoreIdx])
                originalPoreIdxToNewPoreIdx[idx0] = newPoreIdx
                originalPoreIdxToNewPoreIdx[idx1] = newPoreIdx
                newPoreIdx += 1

    finalThroats = []
    for throatIdx, throat in enumerate(originalThroatList):
        if useThroat[throatIdx]:
            tmpVertexIndices = [x[1] for x in vertices]
            poreIdx0 = tmpVertexIndices.index(originalPoreIdxToNewPoreIdx[throat[0]])
            poreIdx1 = tmpVertexIndices.index(originalPoreIdxToNewPoreIdx[throat[1]])
            finalThroats.append([poreIdx0, poreIdx1])

    # Display the grid if desired
    fig = plt.figure()
    sub = fig.add_subplot(1,1,1)

    if showGrid:
        for throat in finalThroats:
            if throat[0] == -1 or throat[1] == -1:
                continue

            x0 = vertices[throat[0]][0][0]
            x1 = vertices[throat[1]][0][0]
            y0 = vertices[throat[0]][0][1]
            y1 = vertices[throat[1]][0][1]
            sub.plot([x0, x1], [y0, y1], "black", marker='o')
        # plt.show()
        fig.savefig("grid_" + str(domainSeed) + ".png")
        plt.close(fig)
        # del plt

    # Write the actual DGF file (cleaned-up)
    with open(gridname + '.dgf', 'w') as gridfile:
        gridfile.write('DGF\nVertex % Coordinates and volumes of the pore bodies\n')
        gridfile.write('parameters 2\n')

        # generate pore radii
        poreRadius = trunc_lognorm(mu=muPore, sigma=sigmaPore, n_samples=int(len(vertices)), x_min=minPoreRadius, x_max=maxPoreRadius, random_state=np.random.RandomState(parameterSeed))
        # np.random.shuffle(poreRadius)

        # calculate the maximum pore radius so that two neighboring pores do not intersect
        maxPoreRadius = [1e9] * len(vertices)
        for throat in finalThroats:
            idx0 = throat[0]
            idx1 = throat[1]
            # get the throat length
            pos0 = vertices[idx0][0]
            pos1 = vertices[idx1][0]
            throatLength = distance.euclidean(pos0, pos1)
            cappedPoreRadius = ((1.0 - minThroatLength) * throatLength) / 2.0

            if cappedPoreRadius < 0.0:
                print "c2c dist: ", throatLength, ", minThroatLength ", (1.0 - minThroatLength), ", cappedRadius ", cappedPoreRadius

            assert cappedPoreRadius > 0.0

            maxPoreRadius[idx0] = min(maxPoreRadius[idx0], cappedPoreRadius)
            maxPoreRadius[idx1] = min(maxPoreRadius[idx1], cappedPoreRadius)

        finalPoreRadius = []

        # write the vertices to the dgf file
        for idx, point in enumerate(vertices):
            pointCoord = point[0]
            if len(pointCoord) < 3:
                pointCoord = np.append(point[0], 0.0)

            poreLabel = -1

            assert poreRadius[idx] > 0.0
            assert maxPoreRadius[idx] > 0.0

            finalPoreRadius.append(min(poreRadius[idx], maxPoreRadius[idx]))

            if(onTop(pointCoord)):
                poreLabel = 2  if pointCoord[0] < 0.5 else 3

            if pointCoord[1] < lowerLeft[1] + eps_:
                poreLabel = 2

            if dimWorld == 2:
                gridfile.write(str(pointCoord[0]) + ' ' + str(pointCoord[1]) + ' ' + str(finalPoreRadius[idx]) + ' ' + str(poreLabel) + '\n')
            else:
                gridfile.write(str(pointCoord[0]) + ' ' + str(pointCoord[1]) + ' ' + str(pointCoord[2]) + ' ' + str(finalPoreRadius[idx]) + ' ' + str(poreLabel) + '\n')

        gridfile.write('#\nSIMPLEX % Connections of the pore bodies (pore throats)\n')
        gridfile.write('parameters 2\n')

        # write the elements to the dgf file
        for idx, throat in enumerate(finalThroats):
            # get the vertex indices
            poreIdx0 = throat[0]
            poreIdx1 = throat[1]

            # get the throat length
            pos0 = vertices[poreIdx0][0]
            pos1 = vertices[poreIdx1][0]
            centerToCenterDistance = distance.euclidean(pos0, pos1)

            effectiveThroatLength = centerToCenterDistance - finalPoreRadius[poreIdx0] - finalPoreRadius[poreIdx1]
            assert effectiveThroatLength > 0.0

            avgThroatRadius = averagedThroatRadius(poreRadiusOne = finalPoreRadius[poreIdx0], poreRadiusTwo = finalPoreRadius[poreIdx1], centerTocenterDist = centerToCenterDistance, n=2)
            assert avgThroatRadius > 0

            # write to the dgf file
            gridfile.write(str(poreIdx0) + ' ' + str(poreIdx1) + ' ' + str(avgThroatRadius) + ' ' + str(effectiveThroatLength)  + '\n')

        gridfile.write('#')

# for i in range(0, 100):
#     makeGrid(i)
#     print "seed", i , "done"
makeGrid(76,55)
