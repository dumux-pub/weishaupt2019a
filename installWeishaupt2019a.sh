# dune-common
# master # 1e7fbb595155445d4b8cae9a2e1dd0b8e887757c # 2018-10-04 13:04:48 +0000 # Markus Blatt
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout master
git reset --hard 1e7fbb595155445d4b8cae9a2e1dd0b8e887757c
cd ..

# dune-geometry
# master # a0d66736f9e0e860a44b99357cc8b6958586399d # 2018-09-06 13:23:24 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout master
git reset --hard a0d66736f9e0e860a44b99357cc8b6958586399d
cd ..

# dune-grid
# master # 8bb63866aa5c0a72f165077038294fa40a5f0816 # 2018-09-23 20:27:15 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout master
git reset --hard 8bb63866aa5c0a72f165077038294fa40a5f0816
cd ..

# dune-localfunctions
# master # 6e57616d38482ae2923a4bcc36f8d2bc5e0a9b76 # 2018-07-02 10:34:33 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout master
git reset --hard 6e57616d38482ae2923a4bcc36f8d2bc5e0a9b76
cd ..

# dune-istl
# master # d74bd9c2e40afc96492d8c354620d6c56e8b3df4 # 2018-08-07 11:55:28 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout master
git reset --hard d74bd9c2e40afc96492d8c354620d6c56e8b3df4
git apply ../weishaupt2019a/umfpack_memory.patch
cd ..

# dune-foamgrid
# master # 0bc28af89974e326005b190d5df4ac8ebb30ebf8 # 2018-09-05 20:05:18 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout master
git reset --hard 0bc28af89974e326005b190d5df4ac8ebb30ebf8
cd ..

# dune-subgrid
# master # e0a3ab0cacaf555dddcb773da031066d46c6b3a4 # 2018-09-03 15:06:09 +0200 # Max Kahnt
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
cd dune-subgrid
git checkout master
git reset --hard e0a3ab0cacaf555dddcb773da031066d46c6b3a4
cd ..

# dumux
# master # a192b99533ca679701d23be71ae7b5eeb1e04413 # 2018-10-17 20:04:12 +0200 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard a192b99533ca679701d23be71ae7b5eeb1e04413
cd ..
